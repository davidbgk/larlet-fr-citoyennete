# Le serment de citoyenneté

> Je jure (ou j’affirme solennellement)  
> Que je serai fidèle  
> Et porterai sincère allégeance  
> À Sa Majesté  
> Le roi Charles Trois  
> Roi du Canada  
> À ses héritiers et successeurs  
> Que j’observerai fidèlement  
> Les lois du Canada  
> Y compris la Constitution  
> Qui reconnaît et confirme les droits  
> Ancestraux ou issus de traités  
> Des Premières Nations, des Inuits et des Métis  
> Et que je remplirai loyalement  
> Mes obligations  
> De citoyen canadien.

[Source avec vidéo](https://www.canada.ca/fr/immigration-refugies-citoyennete/services/citoyennete-canadienne/devenir-citoyen-canadien/ceremonie-citoyennete.html#serment)

!!! info
    La version PDF française du guide fait encore référence à « La reine Elizabeth Deux ».

## Comprendre le serment

Au Canada, nous jurons notre fidélité à une personne humaine
qui nous représente tous, plutôt que de nous engager à servir
un document, une oriflamme ou un territoire. ==Dans notre
monarchie constitutionnelle, la souveraine (reine ou roi)
symbolise à la fois notre Constitution, notre drapeau et notre
pays.== C’est un principe d’une remarquable simplicité, mais
également d’une grande signification : la souveraine
personnifie le Canada tout comme le Canada personnifie la
souveraine.

!!! note
    Ce texte doit être récité pendant la cérémonie (post-examen) et son intitulé est rendu disponible.

## Version anglaise

### The Oath of Citizenship

> I swear (or affirm)  
> That I will be faithful  
> And bear true allegiance  
> To His Majesty  
> King Charles the Third  
> King of Canada  
> His Heirs and Successors  
> And that I will faithfully observe  
> The laws of Canada  
> Including the Constitution  
> Which recognizes and affirms  
> The Aboriginal and treaty rights of  
> First Nations, Inuit and Métis peoples  
> And fulfil my duties  
> As a Canadian citizen.
