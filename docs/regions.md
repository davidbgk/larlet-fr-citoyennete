# Les régions du Canada

Le Canada est le deuxième pays du monde en étendue et son
territoire couvre près de 10 millions de kilomètres carrés. Trois
océans bordent les frontières du Canada : l’océan Pacifique à
l’ouest, l’océan Atlantique à l’est et l’océan Arctique au nord. À
l’extrémité sud du territoire s’étend la frontière entre le Canada
et les États-Unis. Ces deux nations collaborent au maintien
d’une frontière sûre et efficace.

### Les régions du Canada

Le Canada compte de nombreux secteurs géographiques
différents et cinq régions distinctes :

* les provinces de l’Atlantique ;
* le centre du Canada ;
* les provinces des Prairies ;
* la côte Ouest ;
* les territoires du Nord.

### La capitale nationale

Située sur la rivière des Outaouais, la ville d’==Ottawa a été
choisie comme capitale du Canada en 1857 par la reine
Victoria==, l’arrière-arrière-grand-mère de la reine Elizabeth II.
Ottawa est aujourd’hui la quatrième ville en importance au
Canada. La région de la capitale nationale, qui s’étend sur 4 700 
kilomètres carrés autour d’Ottawa, préserve et rehausse le
patrimoine bâti et l’environnement naturel du secteur.

### Les provinces et les territoires

Le Canada compte ==dix provinces et trois territoires==, chacun
ayant sa capitale. Vous devriez connaître la capitale de votre
province ou territoire ainsi que la capitale du Canada.

### La population

Le Canada compte environ 34 millions d’habitants. Bien que la
majorité réside dans les grandes villes, les Canadiens vivent
également dans des villages, en région rurale et partout
ailleurs.

!!! info
    Depuis la rédaction du document, on est passé à 40 millions de canadiens.

RÉGION | PROVINCE OU TERRITOIRE | CAPITALE
-|-|-
**Provinces de l’Atlantique** | Terre-Neuve-et-Labrador | St. John’s
| Île-du-Prince-Édouard | Charlottetown
| Nouvelle-Écosse | Halifax
| Nouveau-Brunswick | Fredericton
**Centre du Canada** | Québec | Québec
| Ontario | Toronto
**Provinces des Prairies** | Manitoba | Winnipeg
| Saskatchewan | Regina
| Alberta | Edmonton
**Côte Ouest** | Colombie-Britannique | Victoria
**Territoires du Nord** | Nunavut | Iqaluit
| Territoires du Nord-Ouest | Yellowknife
| Yukon | Whitehorse

**Ottawa** : La capitale du Canada

## LES PROVINCES DE L’ATLANTIQUE

Grâce à leurs côtes ainsi qu’à leurs richesses naturelles, qui
permettent, entre autres, la pêche, l’agriculture, la foresterie et
l’exploitation minière, les provinces de l’Atlantique jouent un
rôle important dans l’histoire et le développement du Canada.
L’océan Atlantique entraîne des hivers plutôt frais et des étés
frais et humides.

### Terre-Neuve-et-Labrador

Terre-Neuve-et-Labrador, à l’extrême est de l’Amérique du
Nord, occupe son propre fuseau horaire. Au-delà de sa beauté
naturelle, cette province possède un patrimoine distinct,
étroitement lié à la mer. Terre-Neuve est la plus ancienne
colonie de l’Empire britannique et elle a été un atout
stratégique pour le Canada à ses débuts. Elle est depuis
longtemps reconnue pour ses lieux de pêche, ses villages
côtiers de pêcheurs et sa culture bien à elle. De nos jours,
l’exploitation pétrolière et gazière en mer contribue largement à
son économie. Quant au Labrador, ses ressources
hydroélectriques sont immenses.

### Île-du-Prince-Édouard

L’Île-du-Prince-Édouard (Î.-P.-É.), la plus petite des provinces,
est reconnue pour ses plages, sa terre rouge et son
agriculture, particulièrement sa production de pommes de
terre. Berceau de la Confédération, l’Î.-P.-É. est reliée à la terre
ferme par l’un des ponts continus à travées multiples les plus
longs du monde, le pont de la Confédération. C’est à l’Î.-P.-É.
que Lucy Maud Montgomery a situé son fameux roman _Anne…
la maison aux pignons verts_, qui raconte les aventures d’une
petite orpheline aux cheveux roux.

### Nouvelle-Écosse

Parmi les provinces de l’Atlantique, c’est la Nouvelle-Écosse
qui a la plus grande population. Elle est connue pour son
brillant passé à titre de porte d’entrée du Canada et pour sa
baie de Fundy, où les marées atteignent des hauteurs
inégalées dans le monde. On l’identifie également à la
construction navale, à la pêche et à la marine marchande. Sa
capitale, Halifax, plus grand port de la côte Est du Canada, aux
eaux profondes et libres de glace, occupe une place de
premier plan dans la défense et le commerce du côté de
l’Atlantique; elle abrite la plus importante base navale du
Canada. L’exploitation du charbon, la foresterie et l’agriculture
ont façonné la Nouvelle-Écosse, qui aujourd’hui profite
également de l’exploration pétrolière et gazière en mer. Ses
traditions celtiques et gaéliques nourrissent une culture
florissante; chaque année, la Nouvelle-Écosse est l’hôte de
plus de 700 festivals, dont le spectaculaire carrousel militaire
de Halifax.

### Nouveau-Brunswick

Fondée par les loyalistes de l’Empire-Uni, la province du
Nouveau-Brunswick est traversée par les Appalaches et on y
trouve le deuxième réseau hydrographique en importance du
littoral atlantique de l’Amérique du Nord, soit le réseau de la
rivière Saint-Jean. Les principales industries de la province
sont la foresterie, l’agriculture, la pêche, les mines, la
transformation des aliments et le tourisme. Saint John est la
première ville en importance et est le principal centre portuaire
et manufacturier. Moncton est le noyau francophone et
acadien, et Fredericton est la capitale historique. Le Nouveau-
Brunswick est la seule province officiellement bilingue, et
environ le tiers de sa population vit et travaille en français. Les
festivals de rue et la musique traditionnelle font revivre l’histoire
et le patrimoine culturel des fondateurs de la province, soit les
loyalistes et les francophones.

## LE CENTRE DU CANADA

L’Ontario et le Québec forment la région du centre du Canada.
Plus de la moitié de la population du pays vit dans les villes et
les villages du sud du Québec et de l’Ontario, près des Grands
Lacs et du fleuve Saint-Laurent. Cette région est le centre
industriel et manufacturier du Canada. Les hivers sont froids et
les étés sont chauds et humides dans le sud de l’Ontario et du
Québec. Ensemble, l’Ontario et le Québec produisent plus des
trois quarts de tous les biens fabriqués au pays.

### Québec

Le Québec compte près de huit millions d’habitants, dont la
vaste majorité est installée sur les rives du fleuve Saint-Laurent
ou tout près. Plus des trois quarts des habitants du Québec ont
le français comme langue maternelle. Les ressources du
Bouclier canadien ont permis au Québec de développer
d’importantes industries, notamment la foresterie, l’énergie et
l’exploitation minière. Le Québec est le principal producteur de
pâtes et papiers du Canada et ses immenses réserves d’eau
douce en ont fait le plus grand producteur d’hydroélectricité du
pays. Les Québécois sont des chefs de file dans des industries
de pointe comme l’industrie pharmaceutique et l’aéronautique.
Les films, la musique, les oeuvres littéraires et la cuisine du
Québec connaissent un rayonnement international, en
particulier au sein de la Francophonie, une association de pays
francophones. Montréal, qui arrive au deuxième rang des villes
les plus populeuses du Canada et n’est dépassée que par
Paris lorsqu’on dresse la liste des plus grandes villes à
population principalement francophone du monde, est réputée
pour sa diversité culturelle.

### Ontario

Avec plus de 12 millions d’habitants, l’Ontario compte plus d’un
tiers de la population canadienne. Sa population importante et
diversifiée sur le plan culturel, ses ressources naturelles et sa
position stratégique contribuent à la vitalité de son économie.
Toronto est la plus importante ville du Canada et le principal
centre financier du pays. De nombreuses personnes travaillent
dans le secteur des services et dans l’industrie manufacturière,
qui produisent un pourcentage considérable des exportations
du Canada. La région de Niagara est connue pour ses
vignobles, ses vins et ses cultures fruitières. Les fermiers de
l’Ontario élèvent des bovins laitiers et à viande ainsi que de la
volaille, et cultivent des légumes et des céréales. On trouve en
Ontario, fondée par les loyalistes de l’Empire-Uni, la plus
importante population francophone à l’extérieur du Québec,
dont l’histoire est jalonnée de luttes pour préserver sa langue
et sa culture. Les cinq Grands Lacs se situent entre l’Ontario et
les États-Unis; il s’agit du lac Ontario, du lac Érié, du lac Huron,
du lac Michigan (aux États-Unis) et du lac Supérieur, le plus
grand lac d’eau douce au monde.

## LES PROVINCES DES PRAIRIES

Le Manitoba, la Saskatchewan et l’Alberta, qu’on appelle les
provinces des Prairies, possèdent d’immenses ressources
énergétiques et leurs terres agricoles sont parmi les plus
fertiles du monde. La région est essentiellement sèche, avec
des hivers froids et des étés chauds.

### Manitoba

L’économie du Manitoba s’appuie sur l’agriculture, l’exploitation
minière et la production d’hydroélectricité. Winnipeg est la ville
la plus peuplée de la province. C’est dans le quartier _Exchange
District_ de Winnipeg qu’on trouve l’intersection la plus célèbre
du Canada : l’angle de l’avenue Portage et de la rue Main.
Quant au quartier francophone de Winnipeg, Saint-Boniface,
ses 45 000 habitants en font la plus grande communauté
francophone de l’Ouest du Canada. Le Manitoba est aussi une
importante plaque tournante de la culture ukrainienne, 14 pour
100 de ses habitants étant d’origine ukrainienne, et il compte la
plus grande proportion d’Autochtones de toutes les provinces,
à plus de 15 pour 100 de sa population.

### Saskatchewan

La Saskatchewan, autrefois surnommée
« le grenier du monde » et « la province du blé », possède 40
pour 100 des terres arables du Canada. Elle est la plus grande
productrice de céréales et de plantes oléagineuses du pays.
Elle renferme également les plus abondants gisements
d’uranium et de potasse (utilisée dans les fertilisants) du
monde, en plus de produire du pétrole et du gaz naturel. C’est
à Regina, la capitale, que se trouve l’école de la Gendarmerie
royale du Canada. Saskatoon, la ville la plus populeuse de la
Saskatchewan, accueille les sièges sociaux de compagnies
minières et constitue un important centre d’enseignement, de
recherche et de technologie.

### Alberta

L’Alberta est la province la plus peuplée des Prairies. Cette
province, tout comme le célèbre lac Louise situé dans les
montagnes Rocheuses, a été nommée en l’honneur de la
princesse Louise Caroline Alberta, quatrième fille de la reine
Victoria. L’Alberta compte cinq parcs nationaux, dont le parc
national Banff, fondé en 1885. La région sauvage des _badlands_
recèle certains des plus riches gisements de fossiles
préhistoriques et de restes de dinosaures du monde. L’Alberta
est la première productrice de pétrole et de gaz du Canada, et
les sables bitumineux du nord de la province sont une source
énergétique considérable en voie d’exploitation. L’Alberta est
également reconnue pour son agriculture, en particulier ses
énormes exploitations bovines, qui font du Canada l’un des
principaux producteurs bovins du monde.

## LA CÔTE OUEST

La Colombie-Britannique est connue pour ses montagnes
majestueuses et à titre de porte d’entrée du Canada dans le
Pacifique. Des milliards de dollars de marchandises transitent
par le port de Vancouver – le plus grand et le plus fréquenté du
Canada – à destination et en provenance de partout dans le
monde. Grâce aux courants d’air chaud de l’océan Pacifique, la
côte de la Colombie-Britannique bénéficie d’un climat tempéré.

### Colombie-Britannique

La Colombie-Britannique, sur la côte du Pacifique, est la
province canadienne située le plus à l’ouest. Sa population est
de quatre millions d’habitants. Le port de Vancouver nous
ouvre la voie vers l’Asie-Pacifique. Environ la moitié de tous les
biens produits en Colombie-Britannique proviennent de la
foresterie, notamment le bois de sciage, le papier journal et les
produits des pâtes et papiers – l’industrie forestière la plus
importante du Canada. La Colombie-Britannique est également
connue pour son industrie minière, son industrie de la pêche
ainsi que les vergers et l’industrie vinicole de la vallée de
l’Okanagan. Elle possède environ 600 parcs provinciaux,
formant le plus vaste réseau de parcs au Canada. En raison de
la grande taille des communautés asiatiques établies dans la
province, les langues les plus parlées dans les villes après
l’anglais sont le chinois et le punjabi. La capitale, Victoria, est
un centre touristique et sert de port d’attache à la flotte du
Pacifique de la Marine canadienne.

### LES TERRITOIRES DU NORD

Les Territoires du Nord-Ouest, le Nunavut et le Yukon
occupent le tiers de la masse terrestre du Canada; pourtant,
leur population n’est que de 100 000 habitants. On y trouve
des mines d’or, de plomb, de cuivre, de diamants et de zinc, et
des gisements de pétrole et de gaz sont en voie d’exploitation.
Le Nord est souvent appelé « la terre du soleil de minuit » : au
milieu de l’été, le soleil peut briller jusqu’à 24 heures
consécutives, tandis qu’en hiver, il disparaît et l’obscurité règne
pendant trois mois. Les territoires du Nord ont de longs hivers
froids et de courts étés frais. La plus grande partie des
territoires du Nord a pour toile de fond la toundra, vaste plaine
arctique rocailleuse. En raison du climat froid de l’Arctique, la
toundra est dépourvue d’arbres et le sol est constamment gelé.
Certains habitants du Nord tirent encore leur subsistance de la
chasse, de la pêche et de la trappe. Les objets d’art inuit sont
vendus partout au Canada et dans le monde.

### Yukon

Les milliers de mineurs qui se sont rendus au Yukon au cours
de la ruée vers l’or des années 1890 sont célébrés dans la
poésie de Robert W. Service. Les mines continuent d’ailleurs
d’occuper une grande place dans l’économie du Yukon. Le
chemin de fer White Pass and Yukon, qui, à son ouverture en
1900, reliait Skagway – ville de l’État américain voisin, l’Alaska
– à la capitale territoriale de Whitehorse, permet une excursion
touristique spectaculaire parsemée de cols escarpés et de
ponts vertigineux. Le Yukon détient le record de la température
la plus froide jamais enregistrée au Canada (-63 °C).

### Territoires du Nord-Ouest

En 1870, les Territoires du Nord-Ouest (T.N.-O.) ont été formés
à partir de la Terre de Rupert et du Territoire du Nord-Ouest.
La capitale, Yellowknife (20 000 habitants), est surnommée
« capitale nord-américaine du diamant ». Plus de la moitié de
la population est autochtone (Dénés, Inuits et Métis). À 4 200
kilomètres de longueur, le fleuve Mackenzie n’est surpassé en
Amérique du Nord que par le Mississippi sur le plan de
l’étendue, et son bassin hydrographique couvre plus de 1,8
million de kilomètres carrés.

### Nunavut

Le Nunavut (mot signifiant « notre terre » en inuktitut) a été
créé en 1999 à partir de la section est des Territoires du Nord-
Ouest, y compris tout l’ancien district de Keewatin. La capitale,
Iqaluit, s’appelait autrefois Frobisher Bay en l’honneur de
l’explorateur anglais Martin Frobisher, qui, au nom de la reine
Elizabeth 1re, s’est aventuré en 1576 dans cette région non
cartographiée qu’était alors l’Arctique. Les 19 membres de
l’Assemblée législative choisissent un premier ministre et des
ministres par consensus. La population est composée à
environ 85 pour 100 d’Inuits. L’inuktitut y est une langue
officielle et la principale langue d’enseignement.

### Les Rangers canadiens

Le Grand Nord canadien pose des problèmes de sécurité et de
souveraineté. Affrontant des conditions climatiques difficiles
dans une région isolée, les Rangers canadiens – qui font partie
de la Réserve (milice) des Forces canadiennes – y jouent un
rôle clé. De Resolute jusqu’au pôle Nord magnétique, l’hiver en
motoneige et l’été en véhicule tout-terrain, ils utilisent leur
connaissance et leur expérience indigènes du territoire pour
s’assurer que le drapeau du Canada ne cesse de flotter au-
dessus de l’Arctique canadien.
