# Bonjour/Hi!

!!! danger
    Ceci est un site **personnel** qui n’est _pas_ réalisé par le gouvernement du Canada,
    il peut être obsolète. Retrouvez le [guide officiel par ici](https://www.canada.ca/fr/immigration-refugies-citoyennete/organisation/publications-guides/decouvrir-canada.html). Aucune responsabilité, blabla, … à vos risques et périls ! :dragon:

Cet espace est un moyen pour moi d’avoir une version HTML et _responsive_ de la documentation pour éviter d’avoir à consulter ces documents non-web (et lourds) :

1. [Découvrir le Canada : Les droits et responsabilités liés à la citoyenneté](https://www.canada.ca/content/dam/ircc/migration/ircc/francais/pdf/pub/decouvrir.pdf) (PDF, 11,0 Mo)
2. [Découvrir le Canada : Les droits et responsabilités liés à la citoyenneté – Format gros caractères](https://www.canada.ca/content/dam/ircc/migration/ircc/francais/pdf/pub/decouvrir-large.pdf) (PDF, 1,0 Mo)
3. [Découvrir le Canada](https://www.canada.ca/content/dam/ircc/documents/epub/fr/fr-epub-discover-canada.epub) dans le format EPUB (EPUB, 3,96 Mo)

C’est aussi une façon de m’approprier ce contenu.

J’ai gardé le **gras** d’origine dans le texte, le <u>soulignement</u> a été remplace par de l’_italique_ pour éviter de confondre avec des liens sur le web.
Les images n’ont pas été conservées pour l’instant et c’est peut-être une erreur car les légendes pourraient faire l’objet de questions :thinking:.

Tout ce qui a été ==surligné== est de mon fait pour ==apprendre et retenir== les mots/phrases-clés qui me semblent être pertinents pour l’examen.

!!! info
    J’ai aussi ajouté quelques notes personnelles, notamment sur les chiffres/souverains pas à jour dans les documents originaux.

## Conditions de « réussite »

> L’examen pour la citoyenneté en ligne :
> 
> * est en français ou en anglais ;
> * dure 30 minutes ;
> * comporte 20 questions soit :
>     - à choix multiples ;
>     - vrai ou faux.
> 
> Vous devez répondre **correctement à au moins ==15 questions sur 20==** pour réussir à l’examen.
>
> — [[Source](https://www.canada.ca/fr/immigration-refugies-citoyennete/services/citoyennete-canadienne/devenir-citoyen-canadien/passer-examen-citoyennete/enligne.html)]

## Introduction officielle

**Soyez les bienvenus !** Il vous a fallu du courage pour
déménager dans un nouveau pays. Votre décision de
demander la citoyenneté représente une autre étape
importante. Vous vous inscrivez ainsi dans une noble tradition
établie par des générations de pionniers qui vous ont
précédés. Dès que vous aurez rempli toutes les exigences
prévues par la loi, nous espérons vous accueillir parmi les
citoyens canadiens, avec tous les droits et toutes les
responsabilités liés à la citoyenneté.

Le Canada a accueilli des générations de nouveaux arrivants
afin qu’ils l’aident à bâtir une société libre, respectueuse des
lois et prospère. Pendant 400 ans, les colons et les immigrants
ont contribué à la diversité et à la richesse de notre pays, fondé
sur une histoire fière et une identité forte.

Le Canada est ==une monarchie constitutionnelle, une
démocratie parlementaire et un État fédéral==. Les Canadiens
sont unis par un engagement commun envers la ==primauté du
droit== et les institutions d’un ==gouvernement parlementaire==.
Les Canadiens sont fiers de leur identité et ils ont fait des
sacrifices pour défendre leur mode de vie. En venant au
Canada et en entreprenant ces démarches importantes en vue
d’obtenir la citoyenneté canadienne, vous continuez à écrire
l’histoire de notre pays.

Pour obtenir la citoyenneté canadienne, si vous avez de 18 à
54 ans, vous devez posséder une connaissance suffisante du
français ou de l’anglais. Vous devez aussi vous familiariser
avec la procédure de vote, l’histoire, les symboles, les
institutions démocratiques et la géographie du Canada, ainsi
qu’avec les droits et responsabilités liés à la citoyenneté.

Les citoyens canadiens jouissent de nombreux droits, mais ils
ont aussi des responsabilités. Ils doivent respecter les lois du
Canada ainsi que les droits et les libertés des autres.

Le présent guide vous aidera à vous préparer à obtenir la
citoyenneté canadienne. **Bonne chance !**
