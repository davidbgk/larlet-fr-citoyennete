# L’économie canadienne

## UNE NATION COMMERÇANTE

Le Canada est depuis toujours une nation commerçante et le
commerce reste le moteur de sa croissance économique.
Comme Canadiens, nous ne pourrions maintenir notre niveau
de vie sans commercer avec les autres pays.

En 1988, le Canada a négocié un accord de libre-échange
avec les États-Unis. Le Mexique est devenu notre partenaire
en 1994, dans le cadre de l’Accord de libre-échange nord-
américain (ALENA), qui englobe 444 millions de personnes et
a représenté en 2008 plus de un billion de dollars en
commerce de marchandises.

Aujourd’hui, le Canada compte parmi les dix économies les
plus importantes de la planète et fait partie du G8, le groupe
des huit principaux pays industrialisés, avec les États-Unis,
l’Allemagne, le Royaume-Uni, l’Italie, la France, le Japon et la
Russie.

## L’ÉCONOMIE CANADIENNE COMPREND TROIS GRANDS SECTEURS INDUSTRIELS :

* Le **secteur des services** fournit des milliers d’emplois
dans des domaines tels que le transport, l’éducation, les
soins de santé, la construction, les opérations bancaires,
les communications, la vente au détail, le tourisme et
l’administration gouvernementale. Plus de 75 pour 100
des travailleurs canadiens occupent maintenant un emploi
dans le secteur des services.
* Le **secteur manufacturier** produit des biens qui sont
vendus au Canada et partout dans le monde; citons, entre
autres, le papier, le matériel de haute technologie, la
technologie aérospatiale, les automobiles, l’équipement,
les aliments et les vêtements. Les États-Unis sont notre
plus important partenaire commercial à l’échelle
internationale.
* Le **secteur de l’exploitation des ressources naturelles**
comprend la foresterie, la pêche, l’agriculture,
l’exploitation minière et l’énergie. Ces industries ont joué
un rôle important dans l’histoire et le développement du
Canada. Aujourd’hui, l’économie de plusieurs régions du
pays repose encore sur l’exploitation des ressources
naturelles, et ces dernières représentent un pourcentage
élevé des exportations du Canada.

Le Canada entretient des liens étroits avec les États-Unis, et
chacun des deux pays est le plus important partenaire
commercial de l’autre. Plus des trois quarts des exportations
canadiennes sont destinées aux États-Unis. En fait, nos deux
pays entretiennent la plus importante relation commerciale
bilatérale du monde. Les chaînes d’approvisionnement
canado-américaines rivalisent avec le reste du monde. Le
Canada exporte chaque année pour des milliards de dollars de
produits énergétiques, de biens industriels, d’équipement,
d’appareils, de produits de l’automobile, de l’agriculture, de la
pêche et de la foresterie ainsi que de biens de consommation.
Des millions de Canadiens et d’Américains traversent chaque
année en toute sécurité ce qu’on appelle couramment « la plus
longue frontière non défendue du monde ».

À Blaine, dans l’État de Washington, l’Arche de la paix, sur
laquelle on peut lire « children of a common mother » (enfants
d’une même mère) et « brethren dwelling together in unity »
(deux frères vivant ensemble dans l’harmonie), symbolise nos
liens étroits et nos intérêts communs.
