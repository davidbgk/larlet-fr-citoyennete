# Les droits et responsabilités liés à la citoyenneté

Tous les citoyens canadiens ont des droits et des
responsabilités, qui nous viennent de notre passé, qui sont
garantis par le droit canadien et qui reflètent nos traditions,
notre identité et nos valeurs communes.

Les règles juridiques du Canada proviennent entre autres ==des
lois adoptées par le Parlement du Canada et les assemblées
législatives provinciales, de la common law, du code civil de la
France et de la tradition constitutionnelle héritée de la Grande-
Bretagne==.

Ensemble, ces règles préservent pour les Canadiens une
tradition de liberté ordonnée vieille de 800 ans, qui remonte à
1215, année de la signature de la **Magna Carta** (aussi appelée
_Grande Charte des libertés_) en Angleterre, et qui comprend :

* la liberté ==de conscience et de religion== ;
* la liberté ==de pensée, de croyance, d’opinion et d’expression==, y compris la liberté de la presse ;
* la liberté ==de réunion pacifique== ;
* la liberté ==d’association==.

L’**habeas corpus**, ou droit de contester une détention illégale
par l’État, est emprunté à la common law britannique.

La Constitution du Canada a été modifiée en 1982 afin
d’inclure la _Charte canadienne des droits et libertés_, dont le
libellé commence ainsi : ==« Attendu que le Canada est fondé sur
des principes qui reconnaissent la suprématie de Dieu et la
primauté du droit ».== Ces mots soulignent l’importance des
traditions religieuses pour la société canadienne ainsi que la
dignité et la valeur de l’être humain.

La _Charte_ résume les libertés fondamentales tout en y ajoutant
d’autres droits. Les plus importants sont :

* La ==liberté de circulation et d’établissement== – Les Canadiens peuvent vivre et travailler n’importe où au Canada, entrer au Canada et en sortir librement, et demander un passeport canadien.
* Les ==droits des peuples autochtones== – Les droits garantis dans la _Charte_ ne portent atteinte en aucun cas aux droits et libertés (acquis par traité ou autres) des peuples autochtones.
* Les ==droits relatifs aux langues officielles== et les droits à l’instruction dans la langue de la minorité – Le français et l’anglais ont un statut égal au Parlement et dans l’ensemble du gouvernement.
* Le ==multiculturalisme== – Il s’agit d’une caractéristique fondamentale de l’identité et du patrimoine canadiens. Les Canadiens sont heureux de vivre ensemble et s’efforcent de respecter le pluralisme et de vivre en harmonie.

## Égalité entre les femmes et les hommes

Au Canada, les hommes et les femmes sont égaux devant la
loi. L’ouverture et la générosité du Canada excluent les
pratiques culturelles barbares qui tolèrent la violence
conjugale, les « meurtres d’honneur », la mutilation sexuelle
des femmes, les mariages forcés, ou d’autres actes de
violence fondée sur le sexe. Les personnes coupables de tels
crimes sont sévèrement punies en vertu des lois pénales du
Canada.

## Responsabilités liées à la citoyenneté

Au Canada, les droits s’accompagnent de responsabilités,
notamment les suivantes :

* **Respecter les lois** – L’un des principes fondateurs du Canada est la primauté du droit. Les individus et les gouvernements sont régis par des lois et non par des mesures arbitraires. Aucune personne ni aucun groupe n’est au-dessus des lois.
* **Répondre à ses propres besoins et à ceux de sa famille** – Il est important pour les Canadiens d’avoir un emploi, de prendre soin de leur famille et de mettre leurs habiletés à contribution. En travaillant fort, les Canadiens obtiennent un sentiment de dignité personnelle et d’estime de soi, en plus de contribuer à la prospérité du Canada.
* **Faire partie d’un jury** – Lorsqu’on vous le demande, la loi vous oblige à le faire. Être membre d’un jury est un privilège et la participation des citoyens à des jurys impartiaux est essentielle au bon fonctionnement du système judiciaire.
* **Voter aux élections** – Voter est non seulement un droit, mais aussi une responsabilité que vous exercez en votant aux élections fédérales, provinciales ou territoriales, et locales.
* **Offrir de l’aide aux membres de la communauté** – Des millions de bénévoles donnent de leur temps aux autres sans être payés pour ce faire, que ce soit en aidant des personnes dans le besoin, en prêtant main-forte à l’école de leur enfant, en travaillant dans une banque d’alimentation ou un autre organisme de bienfaisance, ou en encourageant les nouveaux arrivants à s’intégrer. Le bénévolat est un excellent moyen d’acquérir des compétences utiles et de se faire des amis et des relations.
* **Protéger notre patrimoine et notre environnement** – Tous les citoyens ont la responsabilité d’éviter le gaspillage et la pollution, ainsi que de protéger le patrimoine naturel, culturel et architectural du pays pour les générations à venir.

## Défendre le Canada

Le Canada n’impose pas le service militaire obligatoire.
Toutefois, travailler dans les **Forces canadiennes** (la Marine,
l’Armée de terre et la Force aérienne) est une noble façon
d’apporter sa contribution au Canada et un excellent choix de
carrière (www.forces.ca). Vous pouvez également travailler à
temps partiel dans la Réserve navale ou aérienne ou encore
dans la Milice de votre localité, et acquérir une expérience et
des compétences utiles tout en élargissant votre réseau de
connaissances. Les jeunes peuvent acquérir une discipline, le
sens des responsabilités et des compétences en s’engageant
dans les Cadets (www.cadets.ca).

Vous pouvez aussi faire partie de la Garde côtière ou des
services d’urgence de votre communauté, comme le service de
police ou les pompiers. En contribuant à la protection de votre
communauté, vous suivez les traces des Canadiens qui vous
ont précédé et qui ont fait des sacrifices pour notre pays.
