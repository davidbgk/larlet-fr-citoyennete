# Le Canada moderne

## COMMERCE ET CROISSANCE ÉCONOMIQUE

Le Canada d’après-guerre connaît une prospérité record et
d’importants progrès. Des traités comme l’Accord général sur
les tarifs douaniers et le commerce (GATT), aujourd’hui
l’Organisation mondiale du commerce (OMC), permettent
d’assouplir les politiques commerciales restrictives appliquées
partout dans le monde pendant la Grande Dépression. La
découverte de gisements de pétrole en Alberta en 1947 lance
l’industrie énergétique moderne du Canada. ==En 1951, pour la
première fois, une majorité de Canadiens peuvent se nourrir,
se loger et se vêtir de manière adéquate.== Entre 1945 et 1970,
le Canada se rapproche des États-Unis et d’autres partenaires
commerciaux, et l’économie canadienne devient l’une des plus
florissantes parmi les nations industrialisées. Aujourd’hui, le
niveau de vie des Canadiens – maintenu par leur travail
acharné et leurs échanges commerciaux avec d’autres pays,
notamment les États-Unis – est l’un des plus élevés du monde.

Plus la prospérité du Canada grandit, plus croît sa capacité de
soutenir ses programmes d’aide sociale. On adopte la _Loi
canadienne sur la santé_, qui garantit des éléments communs et
une norme de protection de base ; le gouvernement fédéral
crée, en 1940, l’assurance-chômage (maintenant appelée 
« assurance-emploi ») ; le Programme de la sécurité de la
vieillesse remonte à 1927 ; le Régime de pensions du Canada
et le Régime des rentes du Québec existent depuis 1965. De
plus, les provinces et les territoires offrent une éducation
subventionnée par l’État.

## ENGAGEMENT INTERNATIONAL

Comme l’Australie, la Nouvelle-Zélande et d’autres pays, le
Canada devient progressivement de plus en plus autonome et
est maintenant en mesure de faire des contributions
considérables à l’échelle internationale.

La Guerre froide débute quand plusieurs pays libérés d’Europe
de l’Est deviennent membres d’un bloc communiste contrôlé
par l’Union soviétique sous la dictature de Joseph Staline. Le
Canada s’unit à d’autres pays démocratiques de l’Occident
pour former l’Organisation du traité de l’Atlantique Nord
(OTAN), une alliance militaire, et aux États-Unis pour créer le
Commandement de la défense aérospatiale de l’Amérique du
Nord (NORAD). Le Canada devient membre d’organisations
internationales telles que l’Organisation des Nations Unies
(ONU). Il participe à l’opération de l’ONU en vue de défendre la
Corée du Sud dans la guerre de Corée (1950-1953), avec un
bilan de 500 morts et de 1 000 blessés. Il prend aussi part à de
nombreuses missions de maintien de la paix de l’ONU dans
des pays aussi divers que l’Égypte, Chypre et Haïti, ainsi qu’à
d’autres opérations internationales de sécurité, notamment en
ex-Yougoslavie et en Afghanistan.

## LE CANADA ET LE QUÉBEC

Durant les années qui suivent la guerre, les Canadiens français
s’épanouissent sur le plan tant social que culturel. Les années
1960 sont au Québec une époque de changements rapides
appelée la ==« Révolution tranquille ».== Beaucoup de Québécois
cherchent à se séparer du Canada. En 1963, le Parlement
établit la Commission royale d’enquête sur le bilinguisme et le
biculturalisme, qui aboutit à l’adoption de la _Loi sur les langues
officielles_ (1969), garantissant des services offerts en français
et en anglais par le gouvernement fédéral partout au Canada.
En 1970, le Canada participe à la création de la Francophonie,
une association internationale de pays francophones.

Le mouvement pour la souveraineté du Québec prend de
l’ampleur, mais il est défait lors d’un référendum organisé dans
la province en 1980. Après beaucoup de négociations, en
1982, la Constitution est modifiée sans l’accord du Québec.
Même si le mouvement pour la souveraineté est défait une fois
de plus lors d’un second référendum en 1995, l’autonomie du
Québec dans le Canada suscite encore des débats à l’heure
actuelle et anime en partie la dynamique qui continue de
façonner notre pays.

## UNE SOCIÉTÉ EN ÉVOLUTION

Avec l’évolution des valeurs sociales pendant plus de
cinquante ans, le Canada devient une société plus souple et
plus ouverte. Beaucoup profitent de l’élargissement des
possibilités d’études secondaires et postsecondaires, et de
plus en plus de femmes deviennent des travailleuses
professionnelles.

La plupart des Canadiens d’origine asiatique s’étaient vu
refuser dans le passé le droit de voter aux élections fédérales
et provinciales, mais les derniers d’entre eux à ne pas
posséder ce droit, les Canadiens d’origine japonaise, finissent
par l’obtenir en 1948. ==Les Autochtones, quant à eux,
acquièrent le droit de vote en 1960.== Aujourd’hui, tous les
citoyens âgés d’au moins 18 ans peuvent voter.

Le Canada accueille des milliers de réfugiés fuyant
l’oppression communiste, dont environ 37 000 ayant échappé à
la tyrannie soviétique en Hongrie en 1956. Avec la victoire
communiste de 1975 à la fin de la guerre du Vietnam,
beaucoup de Vietnamiens fuient et plus de 50 000 d’entre eux
cherchent asile au Canada.

L’idée du multiculturalisme, résultat de l’immigration des dix-
neuvième et vingtième siècles, prend un nouvel élan. ==Dès les
années 1960, le tiers des Canadiens ont une origine autre que
britannique ou française== et sont fiers de conserver leur culture
distincte dans la mosaïque canadienne. De nos jours, la
diversité enrichit la vie des Canadiens, surtout dans nos villes.

## LES ARTS ET LA CULTURE AU CANADA

Les artistes canadiens ont une longue histoire de réalisations
qui font la fierté de leurs concitoyens. Originaires de toutes les
régions, ils reflètent et définissent notre culture et nos formes
d’expression créatrice et atteignent la renommée, à la fois chez
nous et à l’étranger.

Les Canadiens contribuent d’une manière importante à la
littérature anglophone et francophone. Les romanciers, les
poètes, les historiens, les éducateurs et les musiciens ont une
influence considérable sur la culture. Parmi les hommes et les
femmes de lettres, citons Stephen Leacock, Louis Hémon, sir
Charles G. D. Roberts, Pauline Johnson, Émile Nelligan,
Robertson Davies, Margaret Laurence et Mordecai Richler.
Des musiciens comme sir Ernest MacMillan et Healey Willan
sont connus au Canada et dans le monde entier. Des écrivains
comme Joy Kogawa, Michael Ondaatje et Rohinton Mistry
diversifient le paysage littéraire canadien.

Dans les arts visuels, le Canada est peut-être davantage
connu historiquement pour le Groupe des sept, fondé en 1920
et à l’origine d’un style de peinture cherchant à reproduire les
paysages sauvages accidentés. Emily Carr peint les forêts et
les vestiges autochtones de la côte Ouest. Les Automatistes
du Québec, notamment Jean-Paul Riopelle, sont des pionniers
de l’art moderne abstrait dans les années 1950. Louis-Philippe
Hébert, du Québec, est un célèbre sculpteur de grands
personnages historiques. Kenojuak Ashevak jette pour sa part
les fondations de l’art inuit moderne par ses gravures, ses
estampes et ses sculptures en pierre de savon.

Avec son réseau de théâtres régionaux et ses troupes
artistiques de renommée internationale, le Canada se taille une
réputation solide et durable dans le monde des arts de la
scène.

Les films de Denys Arcand connaissent du succès au Québec
et à travers le pays, en plus de remporter des prix
internationaux. Les réalisateurs Norman Jewison et Atom
Egoyan sont d’autres grands noms du cinéma canadien. Les
productions télévisées canadiennes font de nombreux adeptes.

Le Canada se distingue aussi dans les sports, tant amateurs
que professionnels, et toutes les provinces et tous les
territoires produisent des athlètes étoiles ainsi que des
médaillés olympiques. C’est le Canadien James Naismith qui
invente le basket-ball en 1891. On trouve des Canadiens
talentueux dans beaucoup de ligues majeures et les équipes
canadiennes de hockey sur glace – sport national du Canada –
dominent le monde. Donovan Bailey établit un record mondial
et obtient deux médailles d’or en course de vitesse aux Jeux
olympiques d’été de 1996, tandis que Chantal Petitclerc
devient championne paralympique mondiale à la course en
fauteuil roulant. L’un des plus grands joueurs de hockey de
tous les temps, Wayne Gretzky, joue pour les Oilers
d’Edmonton de 1979 à 1988.

En 1980, Terry Fox, un Britanno-Colombien qui a perdu sa
jambe droite à la suite d’un cancer à l’âge de 18 ans,
commence une course à travers le pays, intitulée le «
marathon de l’espoir », afin d’amasser des fonds pour la
recherche sur le cancer. Il devient un héros pour les
Canadiens. Malheureusement, il ne réussit pas à terminer sa
traversée et il perd finalement sa bataille contre le cancer, mais
son histoire se poursuit grâce à des marathons annuels de
collecte de fonds qui portent son nom. En 1985, un autre
Britanno-Colombien, Rick Hansen, fait le tour du globe en
fauteuil roulant afin de recueillir des fonds pour la recherche
sur la moelle épinière.

Les découvertes scientifiques et technologiques du Canada
sont réputées internationalement et changent la façon dont le
monde communique et fait des affaires. Marshall McLuhan et
Harold Innis sont des penseurs avant-gardistes. Le Canada est
reconnu par les autres pays pour son excellence en sciences
et en recherche, et des étudiants, chercheurs et entrepreneurs
de calibre international y viennent pour réaliser leurs travaux
de recherche en médecine, en télécommunications et dans
d’autres domaines. Depuis 1989, l’Agence spatiale canadienne
et les astronautes canadiens participent à l’exploration spatiale,
utilisant souvent le bras robotique conçu et construit par des
Canadiens. Gerhard Herzberg (un réfugié de l’Allemagne
nazie), John Polanyi, Sidney Altman, Richard E. Taylor,
Michael Smith et Bertram Brockhouse, des scientifiques
canadiens, remportent le prix Nobel.

## GRANDES DÉCOUVERTS ET INVENTIONS CANADIENNES

Beaucoup de Canadiens ont fait de grandes découvertes et
inventions. Certains des plus célèbres sont énumérés ci-
dessous.

* **Alexander Graham Bell** – a l’idée du téléphone dans sa maison d’été au Canada.
* **Joseph-Armand Bombardier** – invente la motoneige, un véhicule léger pour l’hiver.
* **Sir Sandford Fleming** – invente le système mondial des fuseaux horaires.
* **Mathew Evans et Henry Woodward** – inventent ensemble la première ampoule électrique et vendent le brevet à Thomas Edison, qui la commercialise et passe à l’histoire.
* **Reginald Fessenden** – contribue à l’invention de la radio, en envoyant le premier message vocal sans fil dans le monde.
* **Le D<sup>r</sup> Wilder Penfield** – était un neurochirurgien d’avant-garde à l’Université McGill à Montréal, et on l’appelait « le plus grand Canadien vivant ».
* **Le D<sup>r</sup> John A. Hopps** – invente le premier stimulateur cardiaque, utilisé aujourd’hui pour sauver la vie de personnes atteintes de problèmes cardiaques.
* **SPAR Aérospatiale / Conseil national de recherches** – inventent le Canadarm, un bras robotique utilisé dans l’espace.
* **Mike Lazaridis et Jim Balsillie** – de Research In Motion (RIM), une société de communication sans fil reconnue pour son invention la plus célèbre, l’appareil BlackBerry.

### Vous voulez en savoir plus sur l’histoire du Canada?

**Visitez un musée ou un lieu historique national!** Au moyen
d’objets historiques, d’œuvres d’art, de récits, d’images et de
documents, les musées explorent les événements et les
réalisations qui ont façonné l’histoire du Canada. Vous pouvez
trouver un musée dans presque toutes les villes du Canada et
chaque province et territoire compte des lieux historiques
nationaux, qu’il s’agisse de champs de bataille, de sites
archéologiques, d’édifices ou de lieux sacrés. Pour trouver un
musée ou un lieu historique national dans votre région,
consultez les sites Web du Musée virtuel du Canada et de
Parcs Canada, dont l’adresse figure à la fin du présent guide.

Pour que le Canada demeure un pays prospère et diversifié, il
faut que tous les Canadiens collaborent pour relever les défis
de l’avenir. En effectuant les démarches pour obtenir la
citoyenneté canadienne, vous cherchez à intégrer un pays qui,
grâce à votre participation active, continuera de grandir et de
prospérer.

**Quelle sera votre contribution au Canada ?**

