# Hymne national : Ô Canada

Au Québec, c’est la version bilingue avec le français en premier :

> Ô Canada! Terre de nos aïeux,  
> Ton front est ceint de fleurons glorieux !  
> Car ton bras sait porter l’épée,  
> Il sait porter la croix !  
> Ton histoire est une épopée  
> Des plus brillants exploits.  
> God keep our land glorious and free !  
> O Canada, we stand on guard for thee.  
> O Canada, we stand on guard for thee.  

[Source avec audio](https://www.canada.ca/fr/immigration-refugies-citoyennete/services/citoyennete-canadienne/devenir-citoyen-canadien/ceremonie-citoyennete.html#serment).

<figure>
  <figcaption>Écouter l’hymne en français :</figcaption>
  <audio controls src="/static/audios/o-canada-bil-fra.mp3">
    <a href="/static/audios/o-canada-bil-fra.mp3"> Télécharger le fichier son </a>
  </audio>
</figure>

Dans toutes les autres provinces et tous les territoires, c’est la version bilingue avec l’anglais en premier :

> O Canada! Our home and native land!  
> True patriot love in all of us command.  
> Car ton bras sait porter l’épée,  
> Il sait porter la croix!  
> Ton histoire est une épopée  
> Des plus brillants exploits.  
> God keep our land glorious and free!  
> O Canada, we stand on guard for thee.  
> O Canada, we stand on guard for thee.

<figure>
  <figcaption>Écouter l’hymne en anglais :</figcaption>
  <audio controls src="/static/audios/o-canada-bil-eng.mp3">
    <a href="/static/audios/o-canada-bil-eng.mp3"> Télécharger le fichier son </a>
  </audio>
</figure>


!!! note
    Cet hymne peut être récité pendant la cérémonie (post-examen) et son intitulé est rendu disponible dans les deux langues.
