# Les symboles canadiens

Le Canada a beaucoup de symboles importants – des objets,
des événements et des personnes qui ont un sens particulier.
Ensemble, ils aident à expliquer ce que signifie être Canadien
et à façonner notre identité nationale. Des symboles canadiens
importants sont reproduits tout au long de cette brochure.

## LA COURONNE DU CANADA

La Couronne est un symbole de l’État au Canada depuis 400
ans. Le Canada est une monarchie constitutionnelle à part
entière depuis la Confédération en 1867, pendant le règne de
la reine Victoria. La reine Elizabeth II est devenue la reine du
Canada en 1952 et a célébré son jubilé d’or en tant que
souveraine en 2002. Son jubilé de diamant (60 ans de règne) a
été célébré en 2012. La Couronne est un symbole du
gouvernement, y compris du Parlement, des assemblées
législatives, des tribunaux, des services de police et des forces
canadiennes.

## LES DRAPEAUX AU CANADA

Un nouveau drapeau canadien a été hissé pour la première
fois en 1965. Le motif rouge-blanc-rouge s’inspire du drapeau
du Collège militaire royal de Kingston, fondé en 1876. Le rouge
et le blanc sont les couleurs de la France et de l’Angleterre
depuis le Moyen Âge et les couleurs nationales du Canada
depuis 1921. Le drapeau de l’_Union Jack_ est notre drapeau
royal officiel. Le _Red Ensign_ canadien a été le drapeau
canadien pendant environ 100 ans. Les provinces et les
territoires ont aussi des drapeaux qui expriment leurs traditions
distinctes.

## LA FEUILLE D’ÉRABLE

La feuille d’érable est le symbole du Canada le plus connu. Les
feuilles d’érable ont été adoptées comme symbole par les
Canadiens français au dix-huitième siècle, elles figurent sur les
uniformes et les insignes militaires canadiens depuis les
années 1850 et sont gravées sur les pierres tombales de nos
soldats morts au combat et enterrés à l’étranger et au Canada.

## LA FLEUR DE LYS

On prétend que la fleur de lys a été adoptée par le roi français
en 496. Elle a été le symbole de la royauté française pendant
plus de 1 000 ans et aura été aussi celui de la colonie de la
Nouvelle-France. Elle reprend de l’importance au moment de la
Confédération et est incluse dans le Red Ensign canadien. En
1948, le Québec adopte son propre drapeau conçu à partir de
la croix et de la fleur de lys.

## LES ARMOIRIES ET LA DEVISE

Pour exprimer sa fierté nationale après la Première Guerre
mondiale, le Canada a adopté des armoiries officielles et une
devise nationale, _A mari usque ad mare_, ce qui en latin signifie
« d’un océan à l’autre ». Les armoiries contiennent les
symboles de l’Angleterre, de la France, de l’Écosse et de
l’Irlande ainsi que des feuilles d’érable rouges. Aujourd’hui, les
armoiries sont reproduites sur les billets de banque, les
documents gouvernementaux et les édifices publics.

## LES ÉDIFICES DU PARLEMENT

Les tours, les voûtes, les sculptures et les vitraux des édifices
du Parlement expriment les traditions françaises, anglaises et
autochtones ainsi que l’architecture néogothique populaire à
l’époque du règne de la reine Victoria. Les édifices ont été
terminés dans les années 1860. L’édifice du Centre a été
détruit par un incendie accidentel en 1916 et reconstruit en
1922; la Bibliothèque est l’unique partie de l’édifice qui a été
épargnée par les flammes. La Tour de la Paix a été terminée
en 1927 en souvenir de la Première Guerre mondiale. À
l’intérieur de la Tour, la Chapelle du Souvenir contient les
Livres du Souvenir, dans lesquels sont écrits les noms des
soldats, des marins et des aviateurs qui sont morts au service
du Canada dans des guerres ou dans l’exercice de leurs
fonctions.

Les assemblées législatives provinciales sont des trésors
architecturaux. L’Assemblée nationale du Québec est
construite dans le style du Second Empire français, tandis que
les assemblées législatives des autres provinces sont
d’architecture baroque, romane et néoclassique et reflètent
l’héritage gréco-romain de la civilisation occidentale qui a
donné naissance à la démocratie.

## LES SPORTS POPULAIRES

Le hockey, sport de spectacle favori des Canadiens, est
considéré comme le sport d’hiver national. Le hockey sur glace
a vu le jour au Canada au dix-neuvième siècle. Les équipes qui
forment la Ligue nationale de hockey se disputent la Coupe
Stanley, donnée en 1892 par lord Stanley, gouverneur général
du Canada. La Coupe Clarkson, créée en 2005 par Adrienne
Clarkson, la 26e gouverneure générale (et la première d’origine
asiatique), est décernée aux équipes de hockey féminin.
Beaucoup de jeunes Canadiens jouent au hockey dans une
ligue, à l’école ou dans les rues peu passantes (hockey de
ruelle ou de rue) et assistent à des parties de hockey sur glace
avec leurs parents. Les enfants canadiens collectionnent les
cartes de hockey depuis de nombreuses générations.

Le jeu de football canadien est le deuxième sport le plus
populaire (voir p. 54). Le curling, jeu sur glace introduit par les
pionniers écossais, est également populaire. Le jeu de la
crosse, sport ancien joué à l’origine par les Autochtones, est le
sport officiel de l’été. Le soccer, quant à lui, compte plus de
joueurs inscrits que toute autre activité sportive au Canada.

## LE CASTOR

Le castor a été choisi comme symbole de la Compagnie de la
Baie d’Hudson il y a plusieurs siècles. Il devient en 1834
l’emblème de la Société Saint-Jean-Baptiste, association
patriotique canadienne-française, et son image est utilisée par
d’autres groupes. On voit ce rongeur infatigable sur les pièces
de monnaie de cinq cents ainsi que sur les armoiries de la
Saskatchewan et de l’Alberta et sur celles de certaines villes
comme Montréal et Toronto.

## LES LANGUES OFFICIELLES DU CANADA

Le français et l’anglais sont les deux langues officielles et
constituent d’importants symboles d’identité. Les francophones
et les anglophones vivent ensemble dans un climat de
partenariat et de tension créatrice depuis plus de 300 ans.
**Vous devez posséder une connaissance suffisante du
 français ou de l’anglais pour obtenir la citoyenneté
 canadienne.** Cette exigence ne s’applique pas aux candidats
adultes âgés de 55 ans ou plus.

Adoptée par le Parlement en 1969, la _Loi sur les langues
officielles_ vise les trois objectifs principaux suivants :

* établir l’égalité entre le français et l’anglais au sein du Parlement, du gouvernement du Canada et des institutions assujetties à la Loi ;
* maintenir et développer les communautés de langue officielle en situation minoritaire au Canada ;
* promouvoir l’égalité du français et de l’anglais dans la société canadienne.

## L’HYMNE NATIONAL

Le « Ô Canada » a été proclamé [hymne national](hymne.md) en 1980, un
siècle après avoir été chanté pour la première fois dans la ville
de Québec, en 1880. Les francophones et les anglophones du
Canada chantent des versions différentes de l’hymne national.

## L’HYMNE ROYAL

L’hymne royal du Canada, intitulé « Dieu protège la reine (ou le
roi) », peut être joué ou chanté à toute occasion où les
Canadiens veulent honorer le souverain.

> **Dieu protège la reine**  
>
> Dieu protège la Reine!  
> De sa main souveraine!  
> Vive la Reine!  
> Qu’un règne glorieux,  
> Long et victorieux,  
> Rende son peuple heureux,  
> Vive la Reine!

> **God Save the Queen**  
>
> God Save our gracious Queen!  
> Long live our noble Queen!  
> God save the Queen!  
> Send her victorious,  
> Happy and glorious,  
> Long to reign over us,  
> God save the Queen!

## L’ORDRE DU CANADA ET LES AUTRES DISTINCTIONS HONORIFIQUES

Tous les pays ont des façons particulières de reconnaître leurs
citoyens exceptionnels. Les récompenses officielles, ou
distinctions honorifiques, sont présentées sous la forme
_d’ordres_, de _décorations_ et de _médailles_. Après avoir utilisé les
titres et décorations britanniques pendant de nombreuses
années, le Canada a mis en place son propre système de
distinctions honorifiques en créant l’Ordre du Canada en 1967,
année marquant le centenaire de la Confédération.

Si vous connaissez un concitoyen digne d’une reconnaissance
spéciale, vous pouvez soumettre sa candidature. De plus
amples renseignements sur nombre de ces récompenses et
sur la procédure de mise en candidature sont disponibles au
www.gg.ca/document.aspx?id=70?&lan=fra.

## LA CROIX DE VICTORIA

La Croix de Victoria, la plus haute distinction que peuvent
recevoir les Canadiens, sert à reconnaître des actes de
bravoure ou des sacrifices remarquables, ou le dévouement
ultime au devoir, face à l’ennemi. La Croix de Victoria a été
décernée à 96 Canadiens depuis 1854, y compris aux
personnes suivantes :

* Alexander Roberts Dunn, né à York (aujourd’hui Toronto),
a servi en tant que lieutenant dans l’armée britannique à
l’occasion de la charge de la brigade légère à Balaclava
(1854), pendant la guerre de Crimée. Il est le premier
Canadien à avoir reçu la Croix de Victoria.
* Le matelot de 2e classe William Hall, de Horton, en
Nouvelle-Écosse, dont les parents étaient des esclaves
américains, est le premier Noir à avoir mérité la Croix de
Victoria, pour son rôle dans le siège de Lucknow durant la
Rébellion indienne de 1857.
* Le caporal Filip Konowal, né en Ukraine, a montré un
courage exceptionnel pendant la bataille de la cote 70, en 1917. 
Il est le premier membre du Corps canadien n’étant
pas né dans l’Empire britannique à se voir décerner la
Croix de Victoria.
* Le capitaine Billy Bishop, as de l’aviation, est né à Owen
Sound, en Ontario. Il a reçu la Croix de Victoria alors qu’il
servait dans le Royal Flying Corps pendant la Première
Guerre mondiale, et a plus tard été nommé maréchal de
l’Air honoraire de l’Aviation royale du Canada.
* Le capitaine Paul Triquet, de Cabano, au Québec, s’est
vu décerner la Croix de Victoria pour avoir mené ses
hommes et une poignée de chars d’assaut dans l’attaque
de Casa Berardi en Italie, en 1943, pendant la Seconde
Guerre mondiale. Il est plus tard devenu brigadier.
* Le lieutenant Robert Hampton Gray, pilote dans la marine
né à Trail, en Colombie-Britannique, a perdu la vie en
contribuant à bombarder et à faire sombrer un navire de
combat japonais en août 1945, quelques jours avant la fin
de la Seconde Guerre mondiale. Il est le dernier Canadien
à ce jour à avoir reçu la Croix de Victoria.

## JOURS FÉRIÉS NATIONAUX ET AUTRES DATES IMPORTANTES

Jour | Date
-|-
Jour de l’An | 1er janvier
Journée sir John A. Macdonald | 11 janvier
Vendredi saint | Vendredi précédant immédiatement le dimanche de Pâques
Lundi de Pâques | Lundi suivant immédiatement le dimanche de Pâques
Jour de Vimy | 9 avril
Fête de Victoria | Lundi précédant le 25 mai (anniversaire de la souveraine)
Fête nationale (Québec) | 24 juin (fête de la Saint-Jean-Baptiste)
Fête du Canada | 1er juillet
Fête du Travail | Premier lundi de septembre
Action de grâces | Deuxième lundi d’octobre
Jour du Souvenir | 11 novembre
Journée sir Wilfrid Laurier | 20 novembre
Noël | 25 décembre
Lendemain de Noël | 26 décembre
