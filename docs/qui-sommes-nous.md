# Qui sommes-nous, les Canadiens ?

Le Canada est reconnu partout dans le monde comme un pays
fort et libre. Les Canadiens sont fiers de leur identité propre.
Nous avons hérité de la plus ancienne tradition
constitutionnelle continue du monde. Nous sommes la seule
==monarchie constitutionnelle== d’Amérique du Nord. Nos
institutions préservent un engagement envers ==les valeurs de la
_paix_, de _l’ordre_ et de _bon gouvernement_==, énoncées en 1867
dans le premier document constitutionnel du Canada, l’_Acte de
l’Amérique du Nord britannique_. Leur désir de liberté ordonnée,
leur esprit d’entreprise, leur travail acharné et leur honnêteté
ont permis aux Canadiens de bâtir une société prospère dans
un climat rigoureux, de l’Atlantique au Pacifique et jusqu’au
cercle arctique, tant et si bien que les poètes et les
chansonniers ont salué le Canada comme étant le ==« Great
Dominion »==, qui signifie « grand dominion » ou « grande
puissance ».

Pour comprendre ce que signifie être Canadien, il faut
connaître nos trois peuples fondateurs : les Autochtones, les
Français et les Britanniques.

## LES PEUPLES AUTOCHTONES

On croit que les ancêtres des peuples autochtones sont venus
d’Asie il y a plusieurs milliers d’années. Ils étaient établis ici
bien avant l’arrivée des premiers explorateurs européens en
Amérique du Nord. Les cultures vivantes et diversifiées des
Premières Nations étaient enracinées dans des croyances
religieuses liées à leur relation avec le Créateur, avec leur
milieu naturel et avec les autres Autochtones.

==Les droits autochtones et les droits découlant de traités sont
énoncés dans la Constitution canadienne.== Les droits
territoriaux ont été garantis pour la première fois par la
Proclamation royale de 1763, du roi George III, qui établissait
les bases de la négociation des traités avec les nouveaux
arrivants – traités qui n’ont pas toujours été respectés.

Des années 1800 jusqu’aux années 1980, le gouvernement
fédéral a placé de nombreux enfants autochtones dans des
pensionnats afin de les instruire et de les assimiler à la culture
canadienne dominante. Ces écoles étaient mal financées et les
élèves y vivaient dans la misère, certains étant même
maltraités physiquement. Les langues et les pratiques
culturelles autochtones y étaient pour la plupart interdites. En
2008, Ottawa a présenté des excuses officielles à tous les
anciens élèves des pensionnats indiens.

Dans le Canada d’aujourd’hui, les peuples autochtones
retrouvent leur fierté et leur confiance, et ils ont à leur actif de
grandes réalisations dans les domaines de l’agriculture, de
l’environnement, des affaires et des arts.

### L’unité dans la diversité

==John Buchan, premier baron Tweedsmuir, a été un gouverneur
général du Canada très populaire (1935-1940).== Il a déclaré que
les communautés immigrantes devraient conserver leur
individualité et contribuer chacune à leur façon à l’essence
même de la nation. Selon lui, chacune pourrait apprendre de
l’autre et, tout en entretenant ses propres allégeances et
traditions, n’en chérir pas moins les nouvelles allégeances et
traditions qui naissent de leur collaboration. (Déclaration faite
devant le Canadian Club de Halifax, en 1937).

Aujourd’hui, le terme _peuples autochtones_ désigne trois
groupes distincts :

Le mot **indien** désigne tous les peuples autochtones sauf les
Inuits et les Métis. Depuis les années 1970, on appelle aussi
ces peuples autochtones les ==Premières Nations==. Aujourd’hui,
près de la moitié des membres des Premières Nations vivent
dans environ 600 communautés au sein de réserves, et l’autre
moitié habite hors des réserves, surtout dans les centres
urbains.

Les **Inuits**, terme signifiant ==« le peuple » en langue inuktitute==,
vivent dans de petites communautés réparties un peu partout
dans l’Arctique.

Leur connaissance de la terre, de la mer et de la faune
sauvage leur a permis de s’adapter à l’un des milieux les plus
arides de la planète.

Les **Métis** sont un peuple distinct qui se compose de
personnes nées de l’==union d’Autochtones et d’Européens==. La
plupart vivent dans les provinces des Prairies. Ils viennent de
milieux à la fois francophones et anglophones et parlent leur
propre dialecte, le michif.

**Environ 65 pour 100 des peuples autochtones sont des
Premières Nations, 30 pour 100 des Métis et 4 pour 100,
des Inuits.**

## LES FRANÇAIS ET LES ANGLAIS

La société canadienne moderne est issue en grande partie des
civilisations chrétiennes francophone et anglophone, amenées
d’Europe par les colons. Le français et l’anglais définissent la
réalité quotidienne de la plupart des gens et sont les deux
langues officielles du Canada. Le gouvernement fédéral est
tenu par la loi de fournir des services en français et en anglais
partout au Canada.

Le Canada compte aujourd’hui 18 millions d’anglophones –
personnes dont la langue maternelle est l’anglais – et 7 millions
de francophones – personnes dont la langue maternelle est le
français. Bien que la plupart des francophones vivent au
Québec, un million de francophones vivent en Ontario, au
Nouveau- Brunswick et au Manitoba. Les francophones sont
aussi présents, mais de façon plus limitée, dans les autres
provinces. ==Le Nouveau-Brunswick est la seule province
officiellement bilingue.==

Les **Acadiens** sont les ==descendants de colons français== établis
dès 1604 dans ce qu’on appelle aujourd’hui les Maritimes. De
1755 à 1763, pendant la guerre entre la Grande-Bretagne et la
France, plus des deux tiers des Acadiens ont été déportés hors
de leur patrie. En dépit de cette épreuve, appelée le ==« Grand
Dérangement »==, les Acadiens ont survécu et maintenu leur
identité propre. Aujourd’hui, la culture acadienne est florissante
et elle contribue au dynamisme du Canada français.

Les **Québécois** sont les habitants du Québec, en grande
majorité francophones. La plupart sont les descendants de
8500 immigrants français arrivés au cours des dix-septième et
dix-huitième siècles et conservent une identité, une culture et
une langue qui leur sont uniques. En 2006, la Chambre des
communes a reconnu que ==les Québécois forment une nation
au sein d’un Canada uni.== Un million d’**Anglo-Québécois** ont
des origines remontant à 250 ans et forment un élément
dynamique du tissu social québécois.

Le mode de vie dans les régions anglophones a été largement
défini par des centaines de milliers de colons, de soldats et
d’immigrants **anglais**, **gallois**, écossais et **irlandais**, arrivés
entre le dix-septième siècle et le vingtième siècle. Des
générations de pionniers et de bâtisseurs d’origine britannique,
comme d’autres groupes, ont travaillé, se sont investies et ont
enduré maintes épreuves afin d’établir les fondements de notre
pays. Cela explique en partie pourquoi on appelle
généralement les anglophones (personnes parlant l’anglais)
des _Canadiens anglais_.

### Devenir Canadien

Certains nouveaux Canadiens arrivent de régions ravagées par
des affrontements ou des conflits armés. Quelles que soient
ces expériences, elles ne peuvent aucunement justifier qu’ils
transposent au Canada des préjugés violents, extrêmes ou
haineux. On s’attend à ce que les nouveaux arrivants adoptent
les ==principes démocratiques== lorsqu’ils deviennent des citoyens
canadiens, notamment la ==primauté du droit==.

## LA DIVERSITÉ AU CANADA

Depuis les années 1800, la majorité des Canadiens sont nés
au Canada. Toutefois, le Canada est souvent appelé une _terre
d’immigration_ du fait qu’au cours des deux siècles derniers,
des millions de nouveaux arrivants ont contribué à construire et
à défendre notre mode de vie.

De nombreux groupes ethniques et religieux vivent et
travaillent côte à côte pacifiquement, en fiers Canadiens. Les
principaux groupes sont les Anglais, les Français, les Écossais,
les Irlandais, les Allemands, les Italiens, les Chinois, les
Autochtones, les Ukrainiens, les Hollandais, les Sud-Asiatiques
et les Scandinaves. Depuis les années 1970, la plupart des
immigrants viennent des pays asiatiques.

Des langues non officielles sont parlées dans de nombreux
foyers canadiens. Les ==langues chinoises sont au deuxième
rang== de celles qui sont les plus parlées à la maison, après
l’anglais, dans deux des plus grandes villes du Canada. À
Vancouver, 13 pour 100 de la population parle l’une des
langues chinoises à la maison, et 7 pour 100 à Toronto.

La grande majorité des Canadiens se disent ==chrétiens==. Le
groupe religieux qui compte le plus de fidèles est celui des
catholiques. Il est suivi des Églises issues de la réforme
protestante. Par ailleurs, le nombre de musulmans, de juifs,
d’hindous, de sikhs et de membres d’autres religions ainsi que
de personnes qui déclarent n’appartenir à aucune religion ne
cesse d’augmenter.

Au Canada, l’État s’est traditionnellement allié avec les
communautés religieuses afin de promouvoir le bien-être
social, l’harmonie et le respect mutuel, d’offrir un enseignement
et des soins de santé, de réinstaller les réfugiés et de maintenir
la liberté de religion, d’expression religieuse et de conscience.
La diversité du Canada s’étend aux ==Canadiens gais et
lesbiennes==, qui bénéficient de l’entière protection de la loi et de
l’égalité de traitement aux termes de celle-ci, y compris de
l’accès au mariage civil.

Ensemble, ces groupes diversifiés, qui ont en commun
l’identité canadienne, forment la société multiculturelle
d’aujourd’hui.
