# Les Canadiens et leur système de gouvernement

Trois faits principaux caractérisent le système de
gouvernement du Canada : notre pays est un ==État fédéral==, une
==démocratie parlementaire== et une ==monarchie constitutionnelle==.

## L’ÉTAT FÉDÉRAL

Il y a un gouvernement fédéral, des gouvernements
provinciaux et territoriaux et des administrations municipales
au Canada. Les responsabilités du gouvernement fédéral et
des gouvernements provinciaux ont été définies en 1867 dans
l’_Acte de l’Amérique du Nord britannique_, maintenant connu
sous le nom de _Loi constitutionnelle de 1867_.

Dans notre _État fédéral_, le gouvernement fédéral assume la
responsabilité des affaires de portée nationale et
internationale, entre autres de la défense, de la politique
étrangère, du commerce et des communications entre les
provinces, de la monnaie, de la navigation, du droit criminel et
de la citoyenneté. Les provinces sont responsables des
municipalités, de l’éducation, de la santé, des ressources
naturelles, de la propriété et des droits civils ainsi que des
autoroutes. Le gouvernement fédéral et les provinces se
partagent la responsabilité de l’agriculture et de l’immigration.
Le fédéralisme permet aux différentes provinces d’adopter des
politiques adaptées à leurs populations et de mettre à l’essai
de nouvelles idées et politiques.

Chaque province a sa propre assemblée législative élue,
comme la Chambre des communes à Ottawa. Les trois
territoires du Nord, qui ont de petites populations, n’ont pas le
statut de province, mais leurs gouvernements et leurs
assemblées exécutent bon nombre des mêmes fonctions.

## LA DÉMOCRATIE PARLEMENTAIRE

Dans la _démocratie parlementaire_ du Canada, la population élit
les députés de la Chambre des communes à Ottawa ainsi que
des assemblées législatives provinciales et territoriales. Ces
représentants sont chargés d’adopter les lois, d’approuver et
de surveiller les dépenses et de veiller à ce que le
gouvernement soit responsable. Les ministres du Cabinet sont
responsables devant les représentants élus, c’est-à-dire qu’ils
doivent conserver la « confiance de la Chambre » et doivent
démissionner s’ils sont défaits à l’issue d’un vote de censure.

Le Parlement comprend trois parties : le **souverain** (la reine ou
le roi), le **Sénat** et la **Chambre des communes**. Les
assemblées législatives provinciales sont constituées du
lieutenant-gouverneur et de l’assemblée élue.

Dans le gouvernement fédéral, le **premier ministre** choisit les
ministres du Cabinet et il est responsable des activités et des
politiques du gouvernement. La **Chambre des communes** est
la chambre des représentants, composée de députés élus par
la population, traditionnellement tous les quatre ans. Les
**sénateurs** sont nommés par le gouverneur général sur
recommandation du premier ministre et restent en poste
jusqu’à l’âge de 75 ans. La Chambre des communes et le
Sénat examinent et révisent les **projets de loi** (propositions de
nouvelles lois). Aucun projet de loi ne peut devenir une loi au
Canada avant d’avoir été adopté par les deux Chambres et
d’avoir obtenu la sanction royale, accordée par le gouverneur
général au nom du souverain.

## ADOPTION DES LOIS

### Comment un projet de loi devient une loi – le processus législatif

1. **Première lecture** – On considère que le projet de loi est lu une première fois et il est imprimé.
2. **Deuxième lecture** – Les députés débattent du principe du projet de loi.
3. **Étape de l’étude en comité** – Les membres du comité examinent le projet de loi, article par article.
4. **Étape du rapport** – Les députés peuvent ajouter d’autres amendements.
5. **Troisième lecture** – Les députés débattent du projet de loi et votent.
6. **Sénat** – Le projet de loi suit un processus similaire.
7. **Sanction royale** – Accepté par les deux Chambres, le projet de loi reçoit alors la sanction royale.

Vivant dans une démocratie, les citoyens canadiens ont le droit
et la responsabilité de participer à la prise des décisions qui les
touchent. Il est important pour les Canadiens âgés de 18 ans
ou plus de participer à leur démocratie en votant aux élections
fédérales, provinciales ou territoriales, et municipales.

## LA MONARCHIE CONSTITUTIONNELLE

==Le Canada étant une _monarchie constitutionnelle_, son **chef
d’État** est un souverain héréditaire== (reine ou roi), qui règne
conformément à la Constitution, soit à la primauté du droit. Le
souverain fait partie du Parlement et joue un rôle important,
non partisan en tant que figure centrale de la citoyenneté et de
l’allégeance, d’une manière plus visible lors des visites royales
au Canada. Sa Majesté est le symbole de la souveraineté
canadienne, la gardienne des libertés constitutionnelles et le
reflet de notre histoire. L’exemple de la famille royale qui s’est
toujours dévouée pour la communauté incite les citoyens à
donner le meilleur d’eux-mêmes à leur pays. Comme chef du
Commonwealth, le souverain lie le Canada à 53 autres nations
qui coopèrent pour l’avancement social, économique et
culturel. Parmi les autres monarchies constitutionnelles du
monde, mentionnons le Danemark, la Norvège, la Suède,
l’Australie, la Nouvelle-Zélande, les Pays-Bas, l’Espagne, la
Thaïlande, le Japon, la Jordanie et le Maroc.

Il existe une nette distinction au Canada entre le **chef d’État** –
le souverain – et le **chef du gouvernement** – le premier
ministre, qui dirige réellement le pays.

Le souverain est représenté au Canada par le **gouverneur
général**, qui est nommé par le souverain sur recommandation
du premier ministre, habituellement pour cinq ans. Dans
chacune des dix provinces, le souverain est représenté par le
**lieutenant-gouverneur**, qui est nommé par le gouverneur
général sur recommandation du premier ministre, aussi
habituellement pour cinq ans.

L’interaction entre les trois pouvoirs du gouvernement –
l’exécutif, le législatif et le judiciaire –, qui collaborent, mais
parfois dans un climat de tension créatrice, aide à protéger les
droits et les libertés des Canadiens.

Chaque gouvernement provincial et territorial a une assemblée
législative élue qui adopte les lois provinciales et territoriales.
Les membres de cette assemblée sont appelés membres de
l’Assemblée législative, membres de l’Assemblée nationale,
membres du Parlement provincial ou membres de la Chambre
d’assemblée, selon la province ou le territoire, ou tout
simplement députés.

Dans chaque province, le premier ministre a un rôle similaire à
celui du premier ministre du gouvernement fédéral, tout comme
le lieutenant-gouverneur a un rôle similaire à celui du
gouverneur général. Dans les trois territoires, le **commissaire**
représente le gouvernement fédéral et joue un rôle cérémonial.

## LE SYSTÈME DE GOUVERNEMENT DU CANADA

<figure markdown>
  ![Image title](static/images/systeme-gouvernement.png){ width="500" }
  <figcaption>TODO: une version HTML…</figcaption>
</figure>
