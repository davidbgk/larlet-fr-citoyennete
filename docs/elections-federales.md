# Les élections fédérales

Les Canadiens votent aux élections pour les personnes qu’ils
veulent comme représentants à la Chambre des communes. À
chaque élection, les électeurs peuvent réélire les mêmes
membres de la Chambre des communes ou en choisir de
nouveaux. Les membres de la Chambre des communes sont
aussi appelés membres du Parlement ou députés.

D’après une loi adoptée par le Parlement, des élections
fédérales doivent avoir lieu le troisième lundi d’octobre tous les
quatre ans après les dernières élections générales. Le premier
ministre peut demander au gouverneur général de déclencher
une élection plus tôt.

Le Canada est divisé en 308 zones électorales, appelées
circonscriptions. Une circonscription est une zone
géographique représentée par un député. Les citoyens de
chaque circonscription élisent un député qui siège à la
Chambre des communes. Ce député représente ses électeurs
ainsi que tous les Canadiens.

Les citoyens canadiens âgés de 18 ans ou plus peuvent se
présenter à une élection fédérale. Les personnes qui se
présentent à une élection sont appelées candidats. Il peut y
avoir de nombreux candidats dans une circonscription.

Dans chaque circonscription, les gens votent pour le candidat
et le parti politique de leur choix. Le candidat qui obtient le plus
grand nombre de votes devient le député de cette
circonscription.

## VOTER

Un des privilèges de la citoyenneté canadienne est le droit de
vote. Pour voter lors d’une élection fédérale ou d’un
référendum fédéral, il faut être à la fois :

* citoyen canadien ;
* âgé d’au moins 18 ans le jour du vote ;
* inscrit sur la liste électorale.

Les listes électorales utilisées pendant les élections et les
référendums fédéraux sont produites à partir du Registre
national des électeurs par un organisme neutre du Parlement
appelé Élections Canada. Il s’agit d’une base de données
permanente sur les citoyens canadiens âgés de 18 ans ou plus
qui ont le droit de voter lors d’une élection ou d’un référendum
fédéral.

Quand une élection est déclenchée, Élections Canada envoie
par la poste une carte d’information de l’électeur à chaque
personne dont le nom figure dans le Registre national des
électeurs. La carte indique quand et à quel endroit vous devez
voter ainsi que le numéro de téléphone à composer si vous
avez besoin d’un interprète ou d’autres services spéciaux.

Même si vous choisissez de ne pas faire inscrire votre nom
dans le Registre national des électeurs ou ne recevez pas de
carte d’information de l’électeur, vous pouvez faire ajouter
votre nom à la liste électorale en tout temps, même le jour de
l’élection.

Pour voter le jour de l’élection ou par anticipation, rendez-vous
au bureau de vote indiqué sur votre carte d’information de
l’électeur. (Voir la rubrique sur la procédure de vote.)

## VOTE SECRET

La loi canadienne prévoit le droit à un vote secret. Cela veut
dire que personne ne peut vous regarder voter et que
personne ne devrait regarder pour qui vous avez voté. Vous
pouvez choisir de discuter de votre vote avec d’autres
personnes, mais personne, y compris les membres de votre
famille, votre employeur ou votre représentant syndical, n’a le
droit d’insister pour que vous lui disiez pour qui vous avez voté.
Immédiatement après la fermeture des bureaux de vote, les
membres du personnel électoral comptent les votes et les
résultats sont annoncés à la radio et à la télévision ainsi que
dans les journaux.

## APRÈS UNE ÉLECTION

Habituellement, après une élection, le chef du parti politique
qui a le plus grand nombre de sièges à la Chambre des
communes est invité par le gouverneur général à former le
gouvernement. Après avoir été nommé par le gouverneur
général, le chef de ce parti devient le premier ministre. Si le
parti au pouvoir détient au moins la moitié des sièges à la
Chambre des communes, on dit qu’il s’agit d’un _gouvernement
majoritaire_. Si le parti au pouvoir détient moins de la moitié des
sièges à la Chambre des communes, on dit qu’il forme un
_gouvernement minoritaire_.

Le premier ministre et le parti au pouvoir forment le
gouvernement aussi longtemps qu’ils ont l’appui ou la
_confiance_ de la majorité des députés. Quand la Chambre des
communes vote sur un enjeu important comme le budget, on
considère qu’il s’agit d’une question de confiance. Si la majorité
des députés de la Chambre des communes vote contre une
décision importante du gouvernement, le parti au pouvoir est
défait, ce qui amène habituellement le premier ministre à
demander au gouverneur général, au nom du souverain, de
déclencher une élection.

Le premier ministre choisit les ministres de la Couronne, la
plupart d’entre eux parmi les députés de la Chambre des
communes. Les ministres du Cabinet ont la responsabilité de
diriger les ministères du gouvernement fédéral. Le premier
ministre et les ministres du Cabinet forment ce qu’on appelle le
Cabinet et prennent les décisions importantes sur la façon de
diriger le pays. Ils préparent le budget et proposent la plupart
des nouvelles lois. Leurs décisions peuvent être remises en
question par tous les députés de la Chambre des communes.

Les autres partis qui ne sont pas au pouvoir sont appelés les
partis d’opposition. Le parti d’opposition qui a le plus grand
nombre de députés à la Chambre des communes forme
l’opposition officielle, ou La loyale Opposition de Sa Majesté.
Le rôle des partis d’opposition est de s’opposer pacifiquement
aux propositions du gouvernement ou d’essayer de les
améliorer. Il y a actuellement trois grands partis politiques
représentés à la Chambre des communes : le Parti
conservateur, le Nouveau Parti démocratique et le Parti libéral.

## LA PROCÉDURE DE VOTE EN PÉRIODE ÉLECTORALE

1. **Carte d’information de l’électeur**
Les électeurs dont les renseignements se trouvent dans le
Registre national des électeurs recevront une carte
d’information de l’électeur. Cette carte confirme que votre nom
est sur la liste électorale et vous indique quand et où voter.
2. **Je n’ai pas reçu de carte**
Si vous ne recevez pas de carte d’information de l’électeur,
téléphonez à votre bureau d’élection local pour vérifier que
votre nom est sur la liste électorale. Si vous n’avez pas le
numéro du bureau, téléphonez à Élections Canada, à Ottawa,
au **1-800-463-6868**.
3. **Le vote par anticipation et bulletin de vote spécial**
Si vous ne pouvez pas ou ne voulez pas voter le jour de
l’élection, vous pouvez voter par anticipation ou par bulletin de
vote spécial, aux dates et lieux inscrits sur votre carte
d’information de l’électeur.
4. **Le jour de l’élection**
Rendez-vous à votre bureau de vote, dont l’adresse figure sur
votre carte d’information de l’électeur. Apportez cette carte
ainsi qu’une preuve de votre identité et de votre adresse au
bureau de vote.
5. **Remplir le bulletin de vote**
Inscrivez un « X » dans le cercle à côté du nom du candidat de
votre choix.
6. **Le vote est secret**
Votre vote est secret. On vous invitera à vous rendre derrière
l’isoloir pour remplir votre bulletin de vote. Une fois le bulletin
rempli, pliez-le et présentez-le au personnel de scrutin.
7. **L’urne**
Le personnel de scrutin détachera le numéro du bulletin de
vote et vous remettra votre bulletin pour que vous le déposiez
dans l’urne.
8. **Les résultats de l’élection**
Quand les bureaux de vote ferment, chaque bulletin de vote
est compté et les résultats sont rendus publics. Vous pouvez
voir les résultats à la télévision ou sur le site Web d’Élections
Canada (www.elections.ca).

## LES AUTRES ORDRES DE GOUVERNEMENT AU CANADA

L’administration _municipale_ ou _locale_ joue un rôle important
dans la vie des citoyens. Les administrations municipales ont
habituellement un conseil qui adopte des lois – appelées 
« règlements » – touchant seulement la communauté locale. Le
conseil est habituellement composé d’un maire (ou d’un préfet)
et de conseillers municipaux ou échevins. Les municipalités
sont normalement responsables de la planification urbaine ou
régionale, des rues et des routes, de l’assainissement (comme
la collecte des ordures), du déneigement, de la lutte contre les
incendies, des services ambulanciers et autres services
d’urgence, des installations récréatives, du transport en
commun et de certains services sociaux et de santé locaux. La
plupart des grands centres urbains ont des services de police
municipaux.

Les élections provinciales, territoriales et municipales se
tiennent par vote secret, mais ne suivent pas les mêmes règles
que les élections fédérales. Il est important de vous renseigner
sur les règles électorales de votre province ou territoire et de
votre municipalité afin de pouvoir exercer votre droit de vote.


Gouvernement ou administration     | Représentants élus               | Quelques responsabilités
-----------------------------------|----------------------------------|-------------------------
**Gouvernement fédéral**           | Députés ou membres du Parlement  | Défense nationale
|| Politique étrangère
|| Citoyenneté
|| Maintien de l’ordre
|| Justice criminelle
|| Commerce international
|| Affaires autochtones
|| Immigration (partagée)
|| Agriculture (partagée)
|| Environnement (partagée)
**Gouvernement provincial et territorial** | Députés ou membres de l’Assemblée législative ou | Éducation
| Députés ou Membres de l’Assemblée nationale ou | Soins de santé
| Députés ou Membres du Parlement provincial ou | Ressources naturelles
| Députés ou Membres de la Chambre d’assemblée | Autoroutes
|| Maintien de l’ordre (Ontario et Québec)
|| Propriété et droits civils
|| Immigration (partagée)
|| Agriculture (partagée)
|| Environnement (partagée)
**Administration municipale (locale)** | Maire ou préfet | Services sociaux et de santé communautaires
| Conseillers municipaux ou échevins |  Programmes de recyclage
|| Transport et services publics
|| Déneigement
|| Maintien de l’ordre
|| Lutte contre les incendies
|| Services d’urgence

Les **Premières Nations** ont des chefs de bande et des
conseillers, qui ont de grandes responsabilités dans les
réserves des Premières Nations, notamment au sujet du
logement, des écoles et d’autres services. Il existe plusieurs
organismes autochtones provinciaux, régionaux et nationaux
qui représentent les Premières Nations, les Métis et les Inuits
dans leurs relations avec le gouvernement fédéral et les
gouvernements provinciaux et territoriaux.


## CONNAISSEZ-VOUS BIEN VOTRE GOUVERNEMENT ?

Utilisez ces pages pour prendre des notes et mémoriser les
renseignements importants

TODO: interactif !
