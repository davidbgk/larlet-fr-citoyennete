# L’histoire du Canada

## LES PEUPLES AUTOCHTONES

Lorsque les Européens arrivent au Canada, ils constatent que
toutes les régions sont habitées par des peuples autochtones,
qu’ils appellent « Indiens », car les premiers explorateurs
croyaient avoir atteint les Indes orientales. Ces peuples vivent
de la terre, certains de la chasse et de la cueillette, d’autres, de
l’agriculture. ==Les Hurons-Wendats de la région des Grands
Lacs sont, comme les Iroquois, des agriculteurs et des
chasseurs. Les Cris et les Dénés du Nord-Ouest sont des
chasseurs-cueilleurs. Les Sioux sont des nomades qui suivent
les troupeaux de bisons. Les Inuits se nourrissent des animaux
sauvages de l’Arctique. Les Autochtones de la côte Ouest font
sécher et fumer le poisson pour le conserver.== Les groupes
autochtones se font souvent la guerre pour agrandir leur
territoire, maîtriser les ressources et accroître leur prestige.

L’arrivée des négociants en fourrures, des missionnaires, des
soldats et des colons européens modifie à jamais le mode de
vie autochtone. Un grand nombre d’Autochtones meurent de
maladies transmises par les Européens, contre lesquelles ils
ne sont pas immunisés. Malgré tout, durant les 200 premières
années de leur coexistence, Autochtones et Européens
forment des liens économiques, religieux et militaires solides
qui jettent les bases du Canada.

## LES PREMIERS EUROPÉENS

Les Vikings d’Islande, qui ont colonisé le Groenland il y a 1 000 ans, 
ont aussi atteint le Labrador et l’île de Terre-Neuve. Les
vestiges de leur établissement, l’Anse aux Meadows, sont un
site du patrimoine mondial.

L’exploration européenne commence véritablement en 1497,
avec l’expédition de Jean Cabot, le premier à dessiner une
carte de la côte Est du Canada.

## UN FLEUVE EST EXPLORÉ, LE CANADA TROUVE SON NOM

De 1534 à 1542, Jacques Cartier traverse trois fois l’Atlantique,
revendiquant des terres pour le roi de France, François Ier.
Cartier entend deux guides qu’il a capturés prononcer le ==mot
iroquois _kanata_, qui signifie « village ».== Dès les années 1550,
on voit apparaître le nom _Canada_ sur les cartes.

## LA NOUVELLE-FRANCE ROYALE

En 1604, les explorateurs français Pierre de Monts et Samuel
de Champlain fondent le premier établissement européen au
nord de la Floride – premièrement à l’île Sainte-Croix
(aujourd’hui dans le Maine), puis à Port-Royal, en Acadie
(aujourd’hui en Nouvelle-Écosse). En 1608, Champlain bâtit
une forteresse sur l’emplacement actuel de la ville de Québec.
Les colons doivent résister au climat rigoureux. Grâce à
Champlain, les colons se sont alliés aux Algonquins, aux
Montagnais et aux Hurons, ennemis historiques des Iroquois ;
ces derniers ont formé une confédération de cinq, puis de six
Premières Nations, qui s’est battue contre les Français
pendant un siècle. Les Français et les Iroquois ont conclu la
paix en 1701.

Les Français et les Autochtones collaborent à l’important
commerce de la traite des fourrures, stimulé par la forte
demande pour les peaux de castor en Europe. Des dirigeants
exceptionnels, comme Jean Talon, Monseigneur de Laval et le
comte de Frontenac, bâtissent en Amérique du Nord un empire
français qui s’étend de la baie d’Hudson au golfe du Mexique.

## LA LUTTE POUR UN CONTINENT

En 1670, le roi Charles II d’Angleterre accorde à la Compagnie
de la Baie d’Hudson l’exclusivité du commerce dans le bassin
hydrographique se déversant dans la baie d’Hudson. Durant
les 100 années qui suivent, la Compagnie fait concurrence aux
négociants établis à Montréal. Les hommes habiles et
courageux qui se déplacent en canot, appelés _voyageurs_ ou
_coureurs des bois_, forment de solides alliances avec les
Premières Nations.

Les colonies anglaises établies dès le début du dix-septième
siècle le long de la côte atlantique finissent par devenir plus
riches et plus peuplées que la Nouvelle-France. Au dix-
huitième siècle, la France et la Grande-Bretagne se font la
guerre pour devenir maîtres de l’Amérique du Nord. En 1759,
les Britanniques gagnent la bataille des plaines d’Abraham à
Québec, marquant ainsi la fin de l’Empire français en
Amérique. Les commandants des deux armées, le brigadier
James Wolfe et le marquis de Montcalm, sont tués tandis qu’ils
mènent leurs troupes au combat.

## LA PROVINCE DE QUÉBEC

Après la guerre, la Grande-Bretagne donne à la colonie le nom
de « Province de Québec ». Les francophones catholiques,
appelés _habitants_ ou _Canadiens_, cherchent à préserver leur
mode de vie au sein de l’Empire britannique anglophone dirigé
par des protestants.

## UNE TRADITION D’ACCOMMODEMENT

Afin de mieux administrer la majorité catholique romaine
francophone, le Parlement britannique adopte ==l’_Acte de
Québec_ en 1774.== L’un des fondements constitutionnels du
Canada, l’_Acte de Québec_ adapte les principes des institutions
britanniques à la réalité de la province. Il accorde la liberté
religieuse aux catholiques et leur permet d’exercer des
fonctions officielles, une pratique non autorisée en Grande-
Bretagne à l’époque. L’_Acte de Québec_ prévoit que les règles
juridiques françaises seront de nouveau appliquées pour les
affaires civiles, et que les règles juridiques anglaises
continueront d’être appliquées pour les affaires criminelles.

## LES LOYALISTES DE L’EMPIRE-UNI

En 1776, les treize colonies britanniques au sud du Québec
proclament leur indépendance et forment les États-Unis.
L’Amérique du Nord est de nouveau divisée par la guerre. Plus
de 40 000 personnes fidèles à la Couronne, les « loyalistes »,
fuient l’oppression de la Révolution américaine afin de s’établir
en Nouvelle-Écosse et au Québec. Joseph Brant conduit des
milliers d’Indiens mohawks loyalistes au Canada. Les loyalistes
sont notamment d’origine hollandaise, allemande, britannique,
scandinave, autochtone et d’autres origines, et de confession
presbytérienne, anglicane, baptiste, méthodiste, juive, quaker
et catholique. Quelque 3 000 loyalistes noirs, esclaves ou
affranchis, viennent vers le nord à la recherche d’une vie
meilleure. Par la suite, certains Néo-Écossais noirs, ayant reçu
des terres stériles, se sont rendus en Afrique de l’Ouest en
1792 afin d’y établir Freetown, en Sierra Leone, nouvelle
colonie britannique pour les esclaves affranchis.

### Naissance de la démocratie

Les institutions démocratiques se développent
progressivement et pacifiquement. La première assemblée de
représentants est élue à Halifax, en Nouvelle-Écosse, en 1758.
Vont suivre l’Île-du-Prince-Édouard en 1773 et le Nouveau-
Brunswick en 1785. ==_L’Acte constitutionnel_ de 1791 divise la
Province de Québec== en deux entités, le Haut-Canada
(aujourd’hui l’Ontario), essentiellement loyaliste, protestant et
anglophone, et le Bas-Canada (aujourd’hui le Québec), surtout
catholique et francophone.

L’Acte accorde pour la première fois aux deux Canadas des
assemblées législatives élues par la population. Le nom
Canada devient alors officiel et sera toujours utilisé par la suite.
Les colonies de la côte atlantique et les deux Canadas sont
appelés collectivement « Amérique du Nord britannique ».

## ABOLITION DE L’ESCLAVAGE

L’esclavage a existé dans le monde entier, en Asie, en Afrique,
au Moyen-Orient et jusqu’aux Amériques. Le premier
mouvement en faveur de l’abolition du commerce
transatlantique des esclaves apparaît au sein du Parlement
britannique à la fin des années 1700. En 1793, le Haut-
Canada, dirigé par le lieutenant-gouverneur John Graves
Simcoe, un officier militaire loyaliste, est la première province
de l’Empire à prendre le virage de l’abolition. ==En 1807, le
Parlement britannique interdit la vente et l’achat d’esclaves et
abolit ensuite l’esclavage dans tout l’Empire en 1833.== Des
milliers d’esclaves fuient les États-Unis. Ils suivent « l’étoile du
Nord » et s’établissent au Canada grâce au « chemin de fer
clandestin », un réseau chrétien antiesclavagiste.

## UNE ÉCONOMIE EN CROISSANCE

Les premières entreprises au Canada, formées sous les
régimes français et britannique, se font concurrence pour la
traite des fourrures. La Compagnie de la Baie d’Hudson, dont
les employés sont français, britanniques et autochtones, en
vient à dominer le commerce dans le Nord-Ouest, de Fort
Garry (Winnipeg) et Fort Edmonton à Fort Langley (près de
Vancouver) et Fort Victoria – des postes de traite devenus plus
tard des villes.

Les premières institutions financières voient le jour à la fin du
dix-huitième siècle et au début du dix-neuvième siècle. ==La
Bourse de Montréal est créée en 1832.== Pendant des siècles,
l’économie du Canada repose essentiellement sur l’agriculture
et l’exportation de ressources naturelles, comme la fourrure, le
poisson et le bois de sciage, qui sont transportées par les
routes, les lacs, les rivieres, les fleuves et les canaux.

### La guerre de 1812 : la lutte pour le Canada

Après la défaite de la flotte de Napoléon Bonaparte à la bataille
de Trafalgar (1805), la Royal Navy domine sur la mer. L’Empire
britannique, dont le Canada fait partie, résiste à Bonaparte qui
cherche à dominer l’Europe. Cet affrontement amène les
Britanniques à s’ingérer dans le commerce maritime des
Américains, ce que ces derniers acceptent mal. Convaincus
qu’il sera facile de s’emparer du Canada, les États-Unis
lancent une invasion en juin 1812. Mais les Américains se
trompent. Des volontaires canadiens et des membres des
Premières Nations, dont des Shawnees dirigés par le chef
Tecumseh, aident les soldats britanniques à défendre le
Canada. En juillet, le major-général sir Isaac Brock s’empare
de Detroit, mais il est tué lors d’une attaque américaine à
Queenston Heights, près de Niagara Falls, attaque qui sera
tout de même repoussée avec succès. En 1813, le lieutenant-
colonel Charles de Salaberry et 460 soldats, pour la plupart
des Canadiens français, refoulent 4 000 envahisseurs
américains à Châteauguay, au sud de Montréal. En 1813, les
Américains incendient la résidence du gouverneur général et
les édifices du Parlement à York (aujourd’hui Toronto). En
guise de représailles, en 1814, le major-général Robert Ross
quitte la Nouvelle-Écosse à la tête d’une expédition qui se
solde par l’incendie de la Maison-Blanche et d’autres édifices
publics à Washington D.C. Ross meurt au combat peu après et
est enterré à Halifax avec tous les honneurs militaires.

En 1814, la tentative de conquête du Canada par les
Américains est un échec complet. Les Britanniques mettent en
place un coûteux système de défense au Canada, notamment
les citadelles de Halifax et de Québec, la cale sèche à Halifax
et Fort Henry à Kingston, qui sont aujourd’hui des lieux
historiques populaires. La frontière canado-américaine actuelle
a été en partie tracée à la suite de la guerre de 1812, qui a
permis de garantir que le Canada resterait indépendant des
États-Unis.

## LES RÉBELLIONS DE 1837 ET 1838

Durant les années 1830, les réformateurs du Haut-Canada et
du Bas-Canada estiment que les progrès vers la démocratie
véritable sont trop lents. Certains sont d’avis que le Canada
devrait adopter les valeurs républicaines des Américains ou
même essayer de se joindre aux États-Unis. Lorsque des
rébellions armées se produisent en 1837 et 1838 aux environs
de Montréal et à Toronto, les rebelles n’obtiennent pas l’appui
de la population qu’il leur faudrait pour réussir. Ils sont défaits
par les troupes britanniques et des volontaires canadiens.
Plusieurs rebelles sont pendus ou envoyés en exil; certains de
ces derniers reviendront plus tard au pays.

Lord Durham, un réformateur anglais dépêché pour faire
rapport sur les rébellions, recommande de fusionner le Haut-
Canada et le Bas-Canada et de les doter d’un _gouvernement
responsable_. Cela signifie que les ministres de la Couronne
doivent obtenir le soutien de la majorité des représentants élus
pour gouverner. Suscitant la controverse, lord Durham ajoute
que le moyen le plus rapide pour les Canadiens français de
réaliser des progrès est de s’assimiler à la culture protestante
anglophone. Cette recommandation illustre une
incompréhension totale à l’égard des Canadiens français, qui
cherchent à protéger l’identité distincte du Canada français.

Certains réformateurs, dont sir Étienne-Paschal Taché et sir
George-Étienne Cartier, deviendront plus tard des Pères de la
Confédération, de même qu’un ancien membre des troupes
gouvernementales volontaires du Haut-Canada, sir John A.
Macdonald.

## LE GOUVERNEMENT RESPONSABLE

En 1840, le Haut-Canada et le Bas-Canada sont réunis pour
former la Province du Canada. Des réformateurs comme sir
Louis-Hippolyte La Fontaine et Robert Baldwin, de même que
Joseph Howe en Nouvelle-Écosse, collaborent avec les
gouverneurs britanniques à l’établissement d’un gouvernement
responsable.

La première colonie de l’Amérique du Nord britannique à se
doter d’un gouvernement pleinement responsable est la
Nouvelle-Écosse, en 1847-1848. En 1848-1849, le gouverneur
du Canada-Uni, lord Elgin, avec les encouragements de
Londres, établit un gouvernement responsable.

Ce système est celui que nous avons aujourd’hui : si le
gouvernement perd un vote de confiance à l’assemblée
législative, il doit démissionner. ==La Fontaine==, défenseur de la
démocratie et des droits des francophones, ==devient le premier
chef d’un gouvernement responsable des deux Canadas.==

## LA CONFÉDÉRATION

De 1864 à 1867, les représentants de la Nouvelle-Écosse, du
Nouveau-Brunswick et de la Province du Canada, avec l’appui
des Britanniques, travaillent ensemble pour créer un nouveau
pays. On appelle ces hommes les Pères de la Confédération.
Ils instaurent deux ordres de gouvernement, soit le fédéral et le
provincial. L’ancienne Province du Canada est séparée en
deux nouvelles provinces : l’Ontario et le Québec, qui,
ensemble, avec le Nouveau-Brunswick et la Nouvelle-Écosse,
forment le nouveau pays appelé le Dominion (ou Puissance)
du Canada. Chaque province élit sa propre assemblée
législative et exerce son autorité sur des domaines tels que
l’éducation et la santé.

_L’Acte de l’Amérique du Nord britannique_ est adopté par le
Parlement britannique en 1867. Le Dominion du Canada est
officiellement créé le 1er juillet 1867. Jusqu’en 1982, le 1er juillet
est célébré sous le nom de « Fête du Dominion » afin de
commémorer le jour où le Canada est devenu un dominion
doté d’un gouvernement autonome. Aujourd’hui, cette fête est
officiellement connue sous le nom de « Fête du Canada ».

### Dominion d’un océan à l’autre

C’est en 1864 que sir Leonard Tilley, représentant élu du
Nouveau-Brunswick et l’un des Pères de la Confédération,
suggère le terme Dominion du Canada. Il s’inspire du passage
suivant du psaume 72 de la Bible : « Qu’il domine d’une mer à
l’autre, et du Fleuve jusqu’au bout de la terre! » Ce passage
incarne la vision de la création d’un pays puissant, uni,
prospère et libre, s’étendant sur tout un continent. Ce titre est
inscrit dans la Constitution et la désignation « dominion » qui
signifie « puissance », utilisée pendant plus d’un siècle, fait
toujours partie de notre patrimoine.

### L’élargissement du Dominion

* **1867** – Ontario, Québec, Nouvelle-Écosse, Nouveau-Brunswick
* **1870** – Manitoba, Territoires du Nord-Ouest
* **1871** – Colombie-Britannique
* **1873** – Île-du-Prince-Édouard
* **1880** – Transfert des îles de l’Arctique (aux T.N.-O.)
* **1898** – Territoire du Yukon
* **1905** – Alberta, Saskatchewan
* **1949** – Terre-Neuve-et-Labrador
* **1999** – Nunavut

**Le saviez-vous ?** Dans les années 1920, certains estiment que
les Antilles britanniques (les territoires britanniques dans la mer
des Caraïbes) devraient être annexées au Canada. Cela n’a
pas lieu, mais le Canada et les pays et territoires des Antilles
membres du Commonwealth entretiennent aujourd’hui
d’étroites relations.

### Le premier premier ministre du Canada

En 1867, **sir John Alexander Macdonald**, un des Pères de la
Confédération, devient le premier premier ministre du Canada.
Né le 11 janvier 1815 en Écosse, il arrive dans le Haut-Canada
pendant son enfance. Il pratique comme avocat à Kingston, en
Ontario. C’est un homme politique brillant et un personnage
coloré. Le Parlement a fait du 11 janvier la Journée sir John A.
Macdonald. Son portrait figure sur les billets de dix dollars.

**Sir George-Étienne Cartier** est le principal architecte de la
Confédération issu du Québec. Avocat spécialisé dans le
domaine des chemins de fer, Montréalais, proche allié de
Macdonald et Canadien français patriote, Cartier fait entrer le
Québec dans la Confédération et aide à négocier l’entrée dans
le Canada des Territoires du Nord-Ouest, du Manitoba et de la
Colombie-Britannique.

## CONTESTATION DANS L’OUEST

Quand, en 1869, le Canada prend possession des vastes
régions du Nord-Ouest transférées par la Compagnie de la
Baie d’Hudson, les 12 000 Métis de la rivière Rouge ne sont
pas consultés. En réaction, Louis Riel mène une révolte armée
et s’empare de Fort Garry, la capitale territoriale. L’avenir du
Canada est menacé. Comment le Dominion peut-il s’étendre
d’un océan à l’autre s’il ne domine pas son territoir ?

En 1870, Ottawa envoie des soldats pour reprendre Fort Garry.
Riel s’enfuit aux États-Unis et le Canada crée une nouvelle
province, le Manitoba. Riel est élu au Parlement, mais il
n’occupera jamais son siège. Plus tard, les droits des Métis et
des Indiens sont de nouveau menacés par l’accroissement de
la colonisation vers l’Ouest et, en 1885, une deuxième révolte
dans l’actuelle Saskatchewan mène au procès de Riel et à son
exécution pour haute trahison, jugement auquel s’oppose
fermement le Québec. Beaucoup voient en Riel un héros, un
défenseur des droits des Métis et le père du Manitoba.

En 1873, après la première révolte des Métis, le premier
ministre Macdonald crée la Police à cheval du Nord-Ouest
(PCNO) afin de pacifier l’Ouest et de faciliter les négociations
avec les Indiens. La PCNO fonde Fort Calgary, Fort MacLeod
et d’autres centres qui sont aujourd’hui devenus des villes et
des municipalités. Regina devient son quartier général.
Aujourd’hui, la Gendarmerie royale du Canada (GRC ou «
police montée ») est la force nationale de police et l’un des
symboles les plus connus de notre pays. Certains des héros
les plus colorés du Canada, comme le major-général sir Sam
Steele, sont issus des rangs de la police montée.

### Un chemin de fer d’un océan à l’autre

La Colombie-Britannique intègre le Canada en 1871 après
avoir reçu d’Ottawa la promesse de la construction d’un
chemin de fer jusqu’à la côte Ouest. Le 7 novembre 1885, ce
puissant symbole d’unité est achevé lorsque Donald Smith
(lord Strathcona), administrateur du Chemin de fer Canadien
Pacifique (CFCP) natif de l’Écosse, pose le dernier crampon.
Le projet est financé par des investisseurs américains et
britanniques et construit par des travailleurs européens et
chinois. Plus tard, les Chinois seront victimes de discrimination,
faisant notamment l’objet d’une taxe d’entrée fondée sur la
race. En 2006, le gouvernement du Canada a présenté ses
excuses pour cette politique discriminatoire. Après des années
de travail héroïque, les « rubans d’acier » du CFCP
concrétisent un rêve national.

## LA MIGRATION VERS L’OUEST

L’économie du Canada se développe et s’industrialise durant le
boom économique des années 1890 et du début du vingtième
siècle. À cette époque, un million de Britanniques et un million
d’Américains immigrent au Canada.

Sir Wilfrid Laurier est le premier Canadien français à devenir
premier ministre depuis la Confédération et il encourage
l’immigration vers l’Ouest. Son portrait figure sur les billets de
cinq dollars. Grâce au chemin de fer, 170 000 Ukrainiens, 115
000 Polonais et des dizaines de milliers d’Allemands, de
Français, de Norvégiens, de Suédois et d’autres immigrants
s’établissent dans l’Ouest canadien avant 1914 et y
développent un secteur agricole prospère.

## LA PREMIÈRE GUERRE MONDIALE

La plupart des Canadiens sont fiers de faire partie de l’Empire
britannique. Plus de 7 000 se portent volontaires pour
participer à la guerre d’Afrique du Sud (1899-1902),
communément appelée la guerre des Boers, où plus de 260
d’entre eux trouvent la mort. En 1900, des Canadiens
combattent dans les batailles de Paardeberg (« montagne aux
chevaux ») et de Lillefontein, des victoires qui renforcent la
fierté nationale au Canada.

Quand l’Allemagne attaque la Belgique et la France en 1914 et
que la Grande-Bretagne déclare la guerre, Ottawa constitue le
Corps expéditionnaire canadien (devenu plus tard le Corps
canadien). Plus de 600 000 Canadiens participent à la guerre –
la plupart volontairement – sur une population totale de huit
millions.

Sur le champ de bataille, les soldats canadiens se révèlent
combatifs et audacieux. Le Canada prend part à la tragédie et
au triomphe du front de l’Ouest. Le Corps canadien s’empare
de la crête de Vimy en avril 1917, au prix de 10 000 morts et
blessés, solidifiant ainsi la réputation de bravoure des
Canadiens en tant que « troupes de choc de l’Empire
britannique ». Un officier canadien a affirmé : « C’était tout le
Canada de l’Atlantique au Pacifique qui passait. Pendant ces
quelques minutes, j’ai assisté à la naissance d’un pays. » Le 9
avril est le Jour de la bataille de Vimy.

Malheureusement, de 1914 à 1920, Ottawa a interné plus de 8 000
anciens ressortissants austro-hongrois, principalement des
Ukrainiens de sexe masculin, à titre de « ressortissants d’un
pays ennemi », dans 24 camps de travail partout au pays,
même si la Grande-Bretagne avait déconseillé au Canada de
le faire.

En 1918, sous le commandement du général sir Arthur Currie,
le plus grand soldat canadien, le Corps canadien progresse
aux côtés des soldats français et britanniques durant les cent
derniers jours du conflit. C’est alors qu’a lieu la bataille
victorieuse d’Amiens, le 8 août 1918 – date que les Allemands
ont baptisée « le jour noir de l’armée allemande » –, suivie de
celles d’Arras, du Canal du Nord, de Cambrai et de Mons.
Avec la capitulation de l’Allemagne et de l’Autriche, la guerre
prend fin à l’Armistice, le 11 novembre 1918. Au total, 60 000
Canadiens ont été tués et 170 000, blessés. La guerre a
renforcé à la fois la fierté nationale et la fierté impériale, surtout
dans le Canada anglais.

## LES FEMMES OBTIENNENT LE DROIT DE VOTE

À l’époque de la Confédération, le droit de vote est limité aux
hommes blancs adultes et propriétaires, ce qui est courant
dans la plupart des pays démocratiques de l’époque. Les
efforts des femmes pour obtenir le droit de vote sont connus
sous le nom de « mouvement des suffragettes ». La fondatrice
de ce mouvement au Canada est la Dre Emily Stowe, première
femme canadienne à pratiquer la médecine au Canada. En
1916, le Manitoba devient la première province à accorder le
droit de vote aux femmes.

En 1917, grâce aux efforts mobilisateurs de femmes comme la
Dre Stowe et d’autres suffragettes, le gouvernement fédéral de
sir Robert Borden donne aux femmes le droit de vote aux
élections fédérales – d’abord aux infirmières qui se trouvent au
front, puis aux femmes ayant un lien de parenté avec des
hommes en service militaire actif. En 1918, la plupart des
citoyennes canadiennes d’au moins 21 ans ont le droit de voter
aux élections fédérales. En 1921, Agnes Macphail, fermière et
enseignante, devient la première députée. En raison du travail
de Thérèse Casgrain et d’autres personnes, ==le Québec a
accordé le droit de vote aux femmes en 1940.==

Chaque année, le 11 novembre, jour du Souvenir, les
Canadiens se rappellent les sacrifices des anciens
combattants et des braves qui sont morts au combat, et ce,
dans toutes les guerres livrées jusqu’à aujourd’hui par le
Canada. Les Canadiens portent le coquelicot rouge et
observent un moment de silence à la 11e heure du 11e jour du
11e mois, pour honorer les sacrifices de plus d’un million de
braves hommes et femmes qui ont servi le Canada, et des 110
000 qui ont donné leur vie. Le lieutenant-colonel John McCrae,
médecin militaire canadien, a composé en 1915 le poème « In
Flanders Fields », souvent récité le jour du Souvenir et dont
voici l’adaptation française de Jean Pariseau, intitulée « Au
champ d’honneur » :

> Au champ d’honneur, les coquelicots  
> Sont parsemés de lot en lot  
> Auprès des croix; et dans l’espace  
> Les alouettes devenues lasses  
> Mêlent leurs chants au sifflement  
> Des obusiers.  
> 
> Nous sommes morts  
> Nous qui songions la veille encor’  
> À nos parents, à nos amis,  
> C’est nous qui reposons ici  
> Au champ d’honneur.  
> 
> À vous jeunes désabusés  
> À vous de porter l’oriflamme  
> Et de garder au fond de l’âme  
> Le goût de vivre en liberté.  
> Acceptez le défi, sinon  
> Les coquelicots se faneront  
> Au champ d’honneur.

## L’ENTRE-DEUX-GUERRES

Après la Première Guerre mondiale, l’Empire britannique se
transforme en une association libre d’États connue sous le
nom de Commonwealth britannique des nations. Le Canada
demeure aujourd’hui l’un des principaux membres du
Commonwealth, avec d’autres États successeurs de l’Empire
comme l’Inde, l’Australie et la Nouvelle-Zélande, en plus de
plusieurs pays d’Afrique et des Antilles.

On appelle les « Années folles » les années 1920, une période
de grande prospérité économique et de faible chômage. Les
Années folles se terminent toutefois par le krach boursier de
1929 et laissent place à la Grande Dépression, une décennie
sombre durant laquelle le chômage atteint 27 pour 100 (en
1933) et de nombreuses entreprises sont anéanties. Les
agriculteurs de l’Ouest canadien sont les plus durement
touchés, en raison des prix faibles des grains et d’une terrible
sécheresse.

On réclame de plus en plus du gouvernement qu’il crée un filet
de sécurité sociale avec un salaire minimum, une semaine de
travail normalisée et des programmes comme l’assurance-
chômage. La Banque du Canada, une banque centrale
chargée de gérer la masse monétaire et d’apporter la stabilité
au système financier, est créée en 1934. L’immigration diminue
et beaucoup de réfugiés sont refusés, y compris des Juifs
essayant de fuir l’Allemagne nazie en 1939.

### L’invasion du jour J, le 6 juin 1944

Pour vaincre le nazisme et le fascisme, les Alliés envahissent
le territoire européen occupé par les nazis. Les Canadiens
participent à la libération de l’Italie en 1943-1944. Le 6 juin
1944, date connue sous le nom de « jour J », prenant part
dans le Nord de la France à l’invasion épique de la Normandie,
15 000 soldats canadiens se lancent à l’assaut de la plage
Juno et l’arrachent à l’armée allemande, une grande réalisation
nationale représentée sur cette toile d’Orville Fisher. Ce jour-là,
environ un soldat allié sur dix est canadien. L’Armée
canadienne libère les Pays-Bas en 1944-1945 et c’est en partie
grâce à elle que les Allemands sont forcés de se rendre le 8
mai 1945, mettant ainsi fin à six années de guerre en Europe.

## LA SECONDE GUERRE MONDIALE

La Seconde Guerre mondiale débute en 1939, quand Adolf
Hitler, le dictateur national-socialiste (nazi) de l’Allemagne,
envahit la Pologne et conquiert une grande partie de l’Europe.
Le Canada s’unit à ses alliés démocratiques pour combattre et
vaincre cette tyrannie par la force des armes.

Plus d’un million de Canadiens et de Terre-Neuviens (Terre-
Neuve est à l’époque une entité britannique distincte)
participent à la Seconde Guerre mondiale, soit une proportion
élevée de la population de 11,5 millions d’habitants, et de ce
nombre, 44 000 sont tués.

Les Canadiens combattent bravement et subissent des pertes
dans la défense infructueuse de Hong Kong (1941), attaquée
par l’empire du Japon, et dans un raid raté contre les nazis à
Dieppe, sur la côte française (1942).

L’Aviation royale du Canada participe à la bataille d’Angleterre
et fournit bon nombre des membres d’équipage du
Commonwealth pour les bombardiers et les chasseurs en
Europe. Le Canada contribue plus que tout autre pays du
Commonwealth à l’effort aérien des Alliés; en effet, plus de 130 000 
membres d’équipage alliés sont formés au Canada dans le
cadre du Programme d’entraînement aérien du Commonwealth
britannique.

La Marine royale du Canada connaît son moment de gloire
dans la bataille de l’Atlantique, alors qu’elle protège des
convois de navires marchands contre les sous-marins
allemands. La marine marchande du Canada aide à nourrir, à
vêtir et à réapprovisionner la Grande-Bretagne. À la fin de la
Seconde Guerre mondiale, le Canada dispose de la troisième
marine en importance au monde.

Durant la guerre du Pacifique, le Japon envahit les îles
Aléoutiennes, attaque un phare sur l’île de Vancouver, lance
des ballons piégés au-dessus de la Colombie-Britannique et
des Prairies et maltraite gravement des prisonniers de guerre
canadiens capturés à Hong Kong. Le Japon capitule le 14 août
1945, mettant ainsi fin à quatre années de guerre dans le
Pacifique.

Malheureusement, compte tenu de l’état de guerre et sous la
pression de l’opinion publique en Colombie-Britannique, le
gouvernement fédéral déplace contre leur gré de nombreux
Canadiens d’origine japonaise et procède à la vente de leurs
biens sans les compenser, même si l’armée et la GRC
soutiennent que ces personnes présentent peu de danger pour
le pays. En 1988, le gouvernement du Canada a présenté ses
excuses aux Canadiens d’origine japonaise pour les torts
causés en temps de guerre et a indemnisé les victimes.
