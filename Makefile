.DEFAULT_GOAL := help
RED=\033[0;31m
GREEN=\033[0;32m
ORANGE=\033[0;33m
BLUE=\033[0;34m
NC=\033[0m # No Color

.PHONY: install
install: ## Install the dependencies
	@echo "${GREEN}🤖 Installing dependencies${NC}"
	python3 -m pip install --upgrade pip
	python3 -m pip install --editable .

.PHONY: dev
dev: install ## Install the development dependencies
	python3 -m pip install --editable ".[dev]"

.PHONY: deps
deps: ## Generate dependencies from pyproject.toml
	@echo "${GREEN}🤖 Updating requirements${NC}"
	python3 -m piptools compile --upgrade --quiet --resolver=backtracking --output-file=requirements.txt pyproject.toml
	python3 -m piptools compile --upgrade --quiet --resolver=backtracking --extra=dev --output-file=requirements-dev.txt pyproject.toml


.PHONY: serve
serve: ## Launch a local server to serve the content.
	@mkdocs serve


.PHONY: build
build: ## Build the website for production.
	@mkdocs build


.PHONY: help
help:
	@python3 -c "$$PRINT_HELP_PYSCRIPT" < $(MAKEFILE_LIST)

# See https://daniel.feldroy.com/posts/autodocumenting-makefiles
define PRINT_HELP_PYSCRIPT # start of Python section
import re, sys

output = []
# Loop through the lines in this file
for line in sys.stdin:
	# if the line has a command and a comment start with
	#   two pound signs, add it to the output
	match = re.match(r'^([a-zA-Z_-]+):.*?## (.*)$$', line)
	if match:
		target, help = match.groups()
		output.append("\033[36m%-10s\033[0m %s" % (target, help))
# Sort the output in alphanumeric order
output.sort()
# Print the help result
print('\n'.join(output))
endef
export PRINT_HELP_PYSCRIPT # End of python section
