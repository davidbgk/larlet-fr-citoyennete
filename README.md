# Citoyennete.larlet.fr

## Installation

```
$ git clone https://gitlab.com/davidbgk/larlet-fr-citoyennete.git
$ cd larlet-fr-citoyennete
$ make venv
$ source venv/bin/activate
$ make install dev
$ make serve
```
